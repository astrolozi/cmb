/*
 * Project name:
     ADC_on LEDs (Display the result of ADC on LEDs)
 * Copyright:
     (c) Mikroelektronika, 2011.
 * Revision History:
     20110929:
       - initial release (FJ);
 * Description:
      A simple example of using the ADC library.
      ADC results are displayed on PORTC and PORTD.
 * Test configuration:
     MCU:             PIC18F45K22
                      http://ww1.microchip.com/downloads/en/DeviceDoc/41412D.pdf
     Dev.Board:       EasyPIC7 - ac:ADC
                      http://www.mikroe.com/eng/products/view/757/easypic-v7-development-system/
     Oscillator:      HS-PLL 32.0000 MHz, 8.0000 MHz Crystal
     Ext. Modules:    None.
     SW:              mikroC PRO for PIC
                      http://www.mikroe.com/eng/products/view/7/mikroc-pro-for-pic/
 * NOTES:
     - Turn on PORTC and PORTD LEDs on SW3.3 and SW3.4.
     - To simulate analog input on ADC channel 1, use on-board potentiometer P1
       and place jumper J15 to MCU pin corresponding to ADC channel 1 input.
 */

#include <built_in.h>

unsigned int adc_rd;

sbit LCD_RS at LATB4_bit;
sbit LCD_EN at LATB5_bit;
sbit LCD_D4 at LATB0_bit;
sbit LCD_D5 at LATB1_bit;
sbit LCD_D6 at LATB2_bit;
sbit LCD_D7 at LATB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
char txt[4];

void main() {
  ANSELA = 0x02;             // Configure RA1 pin as analog
  ANSELC = 0;                // Configure PORTC pins as digital
  ANSELD = 0;                // Configure PORTD pins as digital
  
  TRISA = 0x02;              // Set RA1 pin as input
  TRISC = 0x00;              // Set PORTC as output
   // Set PORTD as output
  ANSELB = 0;                        // Configure PORTB pins as digital

  Lcd_Init();                        // Initialize Lcd

  Lcd_Cmd(_LCD_CLEAR);               // Clear display
  Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off
  Lcd_Out(1,1,"Ovo radi");                 // Write text in first row
  IntToStr(ADC_Read(1), &txt);
  
  Lcd_Out(2,1, txt);
  while (1) {
    //IntToStr(ADC_Read(1), &txt);


    delay_ms(10);
  }
}