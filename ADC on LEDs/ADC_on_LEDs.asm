
_main:

;ADC_on_LEDs.c,46 :: 		void main() {
;ADC_on_LEDs.c,47 :: 		ANSELA = 0x02;             // Configure RA1 pin as analog
	MOVLW       2
	MOVWF       ANSELA+0 
;ADC_on_LEDs.c,48 :: 		ANSELC = 0;                // Configure PORTC pins as digital
	CLRF        ANSELC+0 
;ADC_on_LEDs.c,49 :: 		ANSELD = 0;                // Configure PORTD pins as digital
	CLRF        ANSELD+0 
;ADC_on_LEDs.c,51 :: 		TRISA = 0x02;              // Set RA1 pin as input
	MOVLW       2
	MOVWF       TRISA+0 
;ADC_on_LEDs.c,52 :: 		TRISC = 0x00;              // Set PORTC as output
	CLRF        TRISC+0 
;ADC_on_LEDs.c,54 :: 		ANSELB = 0;                        // Configure PORTB pins as digital
	CLRF        ANSELB+0 
;ADC_on_LEDs.c,56 :: 		Lcd_Init();                        // Initialize Lcd
	CALL        _Lcd_Init+0, 0
;ADC_on_LEDs.c,58 :: 		Lcd_Cmd(_LCD_CLEAR);               // Clear display
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;ADC_on_LEDs.c,59 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off
	MOVLW       12
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;ADC_on_LEDs.c,60 :: 		Lcd_Out(1,1,"Ovo radi");                 // Write text in first row
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr1_ADC_on_LEDs+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr1_ADC_on_LEDs+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;ADC_on_LEDs.c,61 :: 		IntToStr(ADC_Read(1), &txt);
	MOVLW       1
	MOVWF       FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       FARG_IntToStr_input+0 
	MOVF        R1, 0 
	MOVWF       FARG_IntToStr_input+1 
	MOVLW       _txt+0
	MOVWF       FARG_IntToStr_output+0 
	MOVLW       hi_addr(_txt+0)
	MOVWF       FARG_IntToStr_output+1 
	CALL        _IntToStr+0, 0
;ADC_on_LEDs.c,63 :: 		Lcd_Out(2,1, txt);
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       _txt+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(_txt+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;ADC_on_LEDs.c,64 :: 		while (1) {
L_main0:
;ADC_on_LEDs.c,68 :: 		delay_ms(10);
	MOVLW       104
	MOVWF       R12, 0
	MOVLW       228
	MOVWF       R13, 0
L_main2:
	DECFSZ      R13, 1, 1
	BRA         L_main2
	DECFSZ      R12, 1, 1
	BRA         L_main2
	NOP
;ADC_on_LEDs.c,69 :: 		}
	GOTO        L_main0
;ADC_on_LEDs.c,70 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
