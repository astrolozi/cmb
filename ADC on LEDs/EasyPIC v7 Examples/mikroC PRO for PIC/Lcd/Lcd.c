/*
 * Project name:
     Lcd_Test (Demonstration of the LCD library routines)
 * Copyright:
     (c) Mikroelektronika, 2011.
 * Revision History:
     20110929:
       - initial release (FJ);
 * Description:
     This code demonstrates how to use LCD 4-bit library. LCD is first
     initialized, then some text is written, then the text is moved.
 * Test configuration:
     MCU:             PIC18F45K22
                      http://ww1.microchip.com/downloads/en/DeviceDoc/41412D.pdf
     Dev.Board:       EasyPIC7 - ac:LCD
                      http://www.mikroe.com/eng/products/view/757/easypic-v7-development-system/
     Oscillator:      HS-PLL 32.0000 MHz, 8.0000 MHz Crystal
     Ext. Modules:    Character Lcd 2x16
                      http://www.mikroe.com/eng/products/view/277/various-components/
     SW:              mikroC PRO for PIC
                      http://www.mikroe.com/eng/products/view/7/mikroc-pro-for-pic/
 * NOTES:
     - Turn on Lcd backlight switch SW4.6. (board specific)
*/

// Lcd module connections
sbit LCD_RS at LATB4_bit;
sbit LCD_EN at LATB5_bit;
sbit LCD_D4 at LATB0_bit;
sbit LCD_D5 at LATB1_bit;
sbit LCD_D6 at LATB2_bit;
sbit LCD_D7 at LATB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End Lcd module connections

char txt1[] = "mikroElektronika";    
char txt2[] = "EasyPIC7";
char txt3[] = "Lcd4bit";
char txt4[] = "example";
int adc_rd;
char i;                              // Loop variable

void Move_Delay() {                  // Function used for text moving
  Delay_ms(500);                     // You can change the moving speed here
}

void main(){
  ANSELB = 0;                        // Configure PORTB pins as digital
  ANSELA = 0x02;             // Configure RA1 pin as analog
  TRISA = 0x02;              // Set RA1 pin as input
  TRISD = 0x00;
  Lcd_Init();                        // Initialize Lcd
  LATD = 0;
  Lcd_Cmd(_LCD_CLEAR);               // Clear display
  Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off

  Lcd_Out(1,1,"23Kohm");


  while(1) {                         // Endless loop
    adc_rd = ADC_Read(1);    // get ADC value from 1st channel
    if(!(adc_rd >= 322 && adc_rd <= 330)){
                Lcd_Cmd(_LCD_CLEAR);
                Lcd_Out(1,1,"Vise srece");
                Lcd_Out(2,1,"drugi put");
    }
    if(adc_rd > 403 && adc_rd < 408){
              Lcd_Cmd(_LCD_CLEAR);
              Lcd_Out(1,1,"Cestitam!");
              for(;;){
                      LATD = 147;
                      delay_ms(1000);
                      LATD = 0;
                      delay_ms(100);
                      LATD = 91;
                      delay_ms(1000);
                      LATD = 0;
                      delay_ms(100);
                      LATD = 240;
                      delay_ms(1000);
                      LATD = 0;
                      delay_ms(100);
                      LATD = 238;
                      delay_ms(1000);
                      latd = 0;
                      delay_ms(3000);

              }
    }


  }
}