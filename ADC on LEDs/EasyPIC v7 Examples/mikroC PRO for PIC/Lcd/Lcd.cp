#line 1 "E:/desktop10/astrologija/ADC on LEDs/EasyPIC v7 Examples/mikroC PRO for PIC/Lcd/Lcd.c"
#line 27 "E:/desktop10/astrologija/ADC on LEDs/EasyPIC v7 Examples/mikroC PRO for PIC/Lcd/Lcd.c"
sbit LCD_RS at LATB4_bit;
sbit LCD_EN at LATB5_bit;
sbit LCD_D4 at LATB0_bit;
sbit LCD_D5 at LATB1_bit;
sbit LCD_D6 at LATB2_bit;
sbit LCD_D7 at LATB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


char txt1[] = "mikroElektronika";
char txt2[] = "EasyPIC7";
char txt3[] = "Lcd4bit";
char txt4[] = "example";
int adc_rd;
char i;

void Move_Delay() {
 Delay_ms(500);
}

void main(){
 ANSELB = 0;
 ANSELA = 0x02;
 TRISA = 0x02;
 TRISD = 0x00;
 Lcd_Init();
 LATD = 0;
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);

 Lcd_Out(1,1,"23Kohm");


 while(1) {
 adc_rd = ADC_Read(1);
 if(!(adc_rd >= 322 && adc_rd <= 330)){
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(1,1,"Vise srece");
 Lcd_Out(2,1,"drugi put");
 }
 if(adc_rd > 403 && adc_rd < 408){
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(1,1,"Cestitam!");
 for(;;){
 LATD = 147;
 delay_ms(1000);
 LATD = 0;
 delay_ms(100);
 LATD = 91;
 delay_ms(1000);
 LATD = 0;
 delay_ms(100);
 LATD = 240;
 delay_ms(1000);
 LATD = 0;
 delay_ms(100);
 LATD = 238;
 delay_ms(1000);
 latd = 0;
 delay_ms(3000);

 }
 }


 }
}
