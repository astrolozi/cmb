
_Move_Delay:

;Lcd.c,49 :: 		void Move_Delay() {                  // Function used for text moving
;Lcd.c,50 :: 		Delay_ms(500);                     // You can change the moving speed here
	MOVLW       21
	MOVWF       R11, 0
	MOVLW       75
	MOVWF       R12, 0
	MOVLW       190
	MOVWF       R13, 0
L_Move_Delay0:
	DECFSZ      R13, 1, 1
	BRA         L_Move_Delay0
	DECFSZ      R12, 1, 1
	BRA         L_Move_Delay0
	DECFSZ      R11, 1, 1
	BRA         L_Move_Delay0
	NOP
;Lcd.c,51 :: 		}
L_end_Move_Delay:
	RETURN      0
; end of _Move_Delay

_main:

;Lcd.c,53 :: 		void main(){
;Lcd.c,54 :: 		ANSELB = 0;                        // Configure PORTB pins as digital
	CLRF        ANSELB+0 
;Lcd.c,55 :: 		ANSELA = 0x02;             // Configure RA1 pin as analog
	MOVLW       2
	MOVWF       ANSELA+0 
;Lcd.c,56 :: 		TRISA = 0x02;              // Set RA1 pin as input
	MOVLW       2
	MOVWF       TRISA+0 
;Lcd.c,57 :: 		TRISD = 0x00;
	CLRF        TRISD+0 
;Lcd.c,58 :: 		Lcd_Init();                        // Initialize Lcd
	CALL        _Lcd_Init+0, 0
;Lcd.c,59 :: 		LATD = 0;
	CLRF        LATD+0 
;Lcd.c,60 :: 		Lcd_Cmd(_LCD_CLEAR);               // Clear display
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;Lcd.c,61 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off
	MOVLW       12
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;Lcd.c,63 :: 		Lcd_Out(1,1,"23Kohm");
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr1_Lcd+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr1_Lcd+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;Lcd.c,66 :: 		while(1) {                         // Endless loop
L_main1:
;Lcd.c,67 :: 		adc_rd = ADC_Read(1);    // get ADC value from 1st channel
	MOVLW       1
	MOVWF       FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       _adc_rd+0 
	MOVF        R1, 0 
	MOVWF       _adc_rd+1 
;Lcd.c,68 :: 		if(!(adc_rd >= 322 && adc_rd <= 330)){
	MOVLW       128
	XORWF       R1, 0 
	MOVWF       R2 
	MOVLW       128
	XORLW       1
	SUBWF       R2, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main23
	MOVLW       66
	SUBWF       R0, 0 
L__main23:
	BTFSS       STATUS+0, 0 
	GOTO        L_main4
	MOVLW       128
	XORLW       1
	MOVWF       R0 
	MOVLW       128
	XORWF       _adc_rd+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main24
	MOVF        _adc_rd+0, 0 
	SUBLW       74
L__main24:
	BTFSS       STATUS+0, 0 
	GOTO        L_main4
	MOVLW       1
	MOVWF       R0 
	GOTO        L_main3
L_main4:
	CLRF        R0 
L_main3:
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main5
;Lcd.c,69 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;Lcd.c,70 :: 		Lcd_Out(1,1,"Vise srece");
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr2_Lcd+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr2_Lcd+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;Lcd.c,71 :: 		Lcd_Out(2,1,"drugi put");
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr3_Lcd+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr3_Lcd+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;Lcd.c,72 :: 		}
L_main5:
;Lcd.c,73 :: 		if(adc_rd > 403 && adc_rd < 408){
	MOVLW       128
	XORLW       1
	MOVWF       R0 
	MOVLW       128
	XORWF       _adc_rd+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main25
	MOVF        _adc_rd+0, 0 
	SUBLW       147
L__main25:
	BTFSC       STATUS+0, 0 
	GOTO        L_main8
	MOVLW       128
	XORWF       _adc_rd+1, 0 
	MOVWF       R0 
	MOVLW       128
	XORLW       1
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main26
	MOVLW       152
	SUBWF       _adc_rd+0, 0 
L__main26:
	BTFSC       STATUS+0, 0 
	GOTO        L_main8
L__main20:
;Lcd.c,74 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;Lcd.c,75 :: 		Lcd_Out(1,1,"Cestitam!");
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr4_Lcd+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr4_Lcd+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;Lcd.c,76 :: 		for(;;){
L_main9:
;Lcd.c,77 :: 		LATD = 147;
	MOVLW       147
	MOVWF       LATD+0 
;Lcd.c,78 :: 		delay_ms(1000);
	MOVLW       41
	MOVWF       R11, 0
	MOVLW       150
	MOVWF       R12, 0
	MOVLW       127
	MOVWF       R13, 0
L_main12:
	DECFSZ      R13, 1, 1
	BRA         L_main12
	DECFSZ      R12, 1, 1
	BRA         L_main12
	DECFSZ      R11, 1, 1
	BRA         L_main12
;Lcd.c,79 :: 		LATD = 0;
	CLRF        LATD+0 
;Lcd.c,80 :: 		delay_ms(100);
	MOVLW       5
	MOVWF       R11, 0
	MOVLW       15
	MOVWF       R12, 0
	MOVLW       241
	MOVWF       R13, 0
L_main13:
	DECFSZ      R13, 1, 1
	BRA         L_main13
	DECFSZ      R12, 1, 1
	BRA         L_main13
	DECFSZ      R11, 1, 1
	BRA         L_main13
;Lcd.c,81 :: 		LATD = 91;
	MOVLW       91
	MOVWF       LATD+0 
;Lcd.c,82 :: 		delay_ms(1000);
	MOVLW       41
	MOVWF       R11, 0
	MOVLW       150
	MOVWF       R12, 0
	MOVLW       127
	MOVWF       R13, 0
L_main14:
	DECFSZ      R13, 1, 1
	BRA         L_main14
	DECFSZ      R12, 1, 1
	BRA         L_main14
	DECFSZ      R11, 1, 1
	BRA         L_main14
;Lcd.c,83 :: 		LATD = 0;
	CLRF        LATD+0 
;Lcd.c,84 :: 		delay_ms(100);
	MOVLW       5
	MOVWF       R11, 0
	MOVLW       15
	MOVWF       R12, 0
	MOVLW       241
	MOVWF       R13, 0
L_main15:
	DECFSZ      R13, 1, 1
	BRA         L_main15
	DECFSZ      R12, 1, 1
	BRA         L_main15
	DECFSZ      R11, 1, 1
	BRA         L_main15
;Lcd.c,85 :: 		LATD = 240;
	MOVLW       240
	MOVWF       LATD+0 
;Lcd.c,86 :: 		delay_ms(1000);
	MOVLW       41
	MOVWF       R11, 0
	MOVLW       150
	MOVWF       R12, 0
	MOVLW       127
	MOVWF       R13, 0
L_main16:
	DECFSZ      R13, 1, 1
	BRA         L_main16
	DECFSZ      R12, 1, 1
	BRA         L_main16
	DECFSZ      R11, 1, 1
	BRA         L_main16
;Lcd.c,87 :: 		LATD = 0;
	CLRF        LATD+0 
;Lcd.c,88 :: 		delay_ms(100);
	MOVLW       5
	MOVWF       R11, 0
	MOVLW       15
	MOVWF       R12, 0
	MOVLW       241
	MOVWF       R13, 0
L_main17:
	DECFSZ      R13, 1, 1
	BRA         L_main17
	DECFSZ      R12, 1, 1
	BRA         L_main17
	DECFSZ      R11, 1, 1
	BRA         L_main17
;Lcd.c,89 :: 		LATD = 238;
	MOVLW       238
	MOVWF       LATD+0 
;Lcd.c,90 :: 		delay_ms(1000);
	MOVLW       41
	MOVWF       R11, 0
	MOVLW       150
	MOVWF       R12, 0
	MOVLW       127
	MOVWF       R13, 0
L_main18:
	DECFSZ      R13, 1, 1
	BRA         L_main18
	DECFSZ      R12, 1, 1
	BRA         L_main18
	DECFSZ      R11, 1, 1
	BRA         L_main18
;Lcd.c,91 :: 		latd = 0;
	CLRF        LATD+0 
;Lcd.c,92 :: 		delay_ms(3000);
	MOVLW       122
	MOVWF       R11, 0
	MOVLW       193
	MOVWF       R12, 0
	MOVLW       129
	MOVWF       R13, 0
L_main19:
	DECFSZ      R13, 1, 1
	BRA         L_main19
	DECFSZ      R12, 1, 1
	BRA         L_main19
	DECFSZ      R11, 1, 1
	BRA         L_main19
	NOP
	NOP
;Lcd.c,94 :: 		}
	GOTO        L_main9
;Lcd.c,95 :: 		}
L_main8:
;Lcd.c,98 :: 		}
	GOTO        L_main1
;Lcd.c,99 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
