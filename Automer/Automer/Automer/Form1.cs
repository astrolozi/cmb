﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Automer
{
    public partial class Form1 : Form
    {
        string currentAngles = "";
        double ub = 100;
        public Form1()
        {
            InitializeComponent();
            comboBox1.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames());
            comboBox2.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames());
            for (int i = 0; i < 100; i++)
            {
                chart1.Series[0].Points.AddY(0);
            }
            chart1.ChartAreas[0].AxisY.Maximum = 1.3;
            chart1.ChartAreas[0].AxisY.Minimum = 1.2;
            
        }
        bool write = false;
        private void button1_Click(object sender, EventArgs e)
        {
            serialPort1.PortName = (string) comboBox1.Text;
            serialPort1.BaudRate = 256000;
            serialPort1.Open();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            serialPort2.PortName = (string) comboBox2.Text;
            serialPort2.BaudRate = 115000;
            serialPort2.Open();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (write)
            {
                write = false;
            }
            else if(serialPort1.IsOpen && serialPort2.IsOpen)
            {
                write = true;
            }
            
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (write)
            {
                string tmp = serialPort1.ReadLine();
                DateTime now = DateTime.Now;
                tmp = tmp.Remove(tmp.Length - 1, 1);
                SetTBLines(now.Hour.ToString() + ":" + now.Minute.ToString() + ":" + now.Second.ToString() + "." +
                    now.Millisecond.ToString() + " " + tmp + "; " + currentAngles);
                currentAngles = "";
               
            }

        }

        delegate void SetTextCallback(string line);

        void SetTBLines(string line)
        {

            if (this.textBox1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetTBLines);
                this.Invoke(d, new object[] { line });
            }
            else
            {
                this.textBox1.AppendText(line +"\r\n");

                /*try
                {
                    var txt = lines[lines.Length - 1];
                    var U = Convert.ToDouble(txt.Split(' ')[3]);
                    var Measuring = Convert.ToInt16(txt.Split(' ')[4]);
                    double tmpUB = 0, UBcnt = 0;
                    bool bias = false;
                    if (txt.Split(' ')[4] == "0")
                    {
                        if (bias)
                        {
                            tmpUB += Convert.ToDouble(txt.Split(' ')[3]);
                            UBcnt++;
                            ub = tmpUB / UBcnt;
                        }
                        else
                        {
                            tmpUB = Convert.ToDouble(txt.Split(' ')[3]);
                            UBcnt = 1;
                            ub = tmpUB;
                        }
                    }
                    else if (txt.Split(' ')[4] == "1" && ub != 100 && Math.Abs(Convert.ToDouble(txt.Split(' ')[3]) - ub) > 10e-4)
                    {
                    }
                    chart1.Series[0].Points.RemoveAt(0);

                    chart1.Series[0].Points.AddY(Convert.ToDouble(txt.Split(' ')[3]));
                    chart1.ResetAutoValues();

                }
                catch (Exception ex)
                {

                }*/
            }

        }

       
        private void serialPort2_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (write)
            {
                string tmp = serialPort2.ReadLine();
                DateTime now = DateTime.Now;
                tmp = tmp.Remove(tmp.Length - 1, 1);
                currentAngles += now.Hour.ToString() + ":" + now.Minute.ToString() + ":" + now.Second.ToString() + "." +
                    now.Millisecond.ToString() + " " + tmp + "; ";
                
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(saveFileDialog1.FileName);

            foreach (var line in textBox1.Lines)
            {
                sw.WriteLine(line);
            }
            sw.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                
                serialPort1.Write("S");
                //serialPort1.Write("R");
            }
        }
    }
}
