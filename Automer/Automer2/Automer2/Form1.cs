﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Automer2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.AcceptButton = button1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var lsts = textBox1.Lines.ToList();
            lsts.Add(DateTime.Now.ToString("hh:mm:ss") + "; " + textBox2.Text);
            textBox1.Lines = lsts.ToArray();
            textBox2.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(saveFileDialog1.FileName + ".companion");
            foreach(var line in textBox1.Lines)
            {
                sw.WriteLine(line);
            }
            sw.Close();
        }
    }
}
