/*
 * Project name:
     MPU_IMU click (Using mikroE's MPU_IMU click board)
 * Copyright:
     (c) Mikroelektronika, 2014.
 * Revision History:
     20140530:
       - initial release (FJ);
 * Description:
      A simple example of using MPU_IMU click board, which will read gyroscope, accelerometer
      and temperature data and write to the UART.
 * Test configuration:
     MCU:             PIC18F45K22
                      http://ww1.microchip.com/downloads/en/DeviceDoc/41412F.pdf
     Dev.Board:       EasyPIC7
                      http://www.mikroe.com/easypic/
     Oscillator:      HS-PLL 32.0000 MHz, 8.0000 MHz Crystal
     Ext. Modules:    MPU_IMU click board - ac:MPU_IMU_click
     SW:              mikroC PRO for PIC
                      http://www.mikroe.com/mikroc/pic/
 * NOTES:
     - Place MPU_IMU click board in the mikroBUS socket 1 on the EasyPIC7 board.
     - Put power supply jumper (J5) on the EasyPIC7 board in the 3.3V position.
 */

#include "MPU_IMU_Register_Map.h"
#include "MPU_IMU_I2C.h"

// Gyro offset
#define Gyrox_offset  3.3
#define Gyroy_offset  0
#define Gyroz_offset  0

// Accel offset
#define Accelx_offset -0.07
#define Accely_offset 0.02
#define Accelz_offset 0.2

// Temperature offset
#define Temp_offset   36.53

// MPU IMU module connections
sbit INT at RB3_bit;
sbit INT_Direction at TRISB3_bit;
// End of MPU IMU module connections
char* tmpTxt[30];
// Variables used
unsigned int raw_data[7];
int gyro_x_temp, gyro_y_temp, gyro_z_temp, accel_x_temp, accel_y_temp, accel_z_temp, temp_raw;
char i, buff, gyro_x_out[15], gyro_y_out[15], gyro_z_out[15], accel_x_out[15], accel_y_out[15], accel_z_out[15], temp_out[15];
float temp, gyro_x, gyro_y, gyro_z, accel_x, accel_y, accel_z;

// Initialize MPU
void Init_MPU(){
  MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_PWR_MGMT_1 , 1, 0x80);
  Delay_ms(100);
  MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_PWR_MGMT_1 , 1, 0x00);
  Delay_ms(100);
  MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_FIFO_EN , 1, 0x78);
  Delay_ms(100);
  MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_INT_ENABLE , 1, 0x10);
  Delay_ms(100);
  MPU_I2C_Read (mpu_I2C_ADDR, mpu_rm_ACCEL_CONFIG, 1, &raw_data);
  Delay_ms(100);
  raw_data[0] |= 0x18;
  MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_ACCEL_CONFIG, 1, &raw_data[0]);
  Delay_ms(100);
  MPU_I2C_Read (mpu_I2C_ADDR, mpu_rm_INT_ENABLE, 1, &raw_data);
  Delay_ms(100);
  raw_data[0] |= 0x11;
  MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_INT_ENABLE, 1, &raw_data[0]);
  Delay_ms(100);
}

// Initialize MCU
void Init_MCU(){
  // Set pins an digital
  ANCON0 = 0xFF;            // All pins to digital
  ANCON1 = 0xFF;
  
  // Set PORTC as output
  TRISC = 0;
  
  // Set IN pin as input
  INT_Direction = 1;

  // Initialize I2C module
  I2C2_Init(400000);
  Delay_ms(100);
  
  // Initialize UART module
  UART1_Init(115200);
  Delay_ms(100);
}

// Extract readings from MPU
void Extract_Readings() {
  // Extract accel readings
  accel_x_temp = raw_data[0];
  accel_x = (float)accel_x_temp / 2048.0;
  accel_y_temp = raw_data[1];
  accel_y = (float)accel_y_temp / 2048.0;
  accel_z_temp = raw_data[2];
  accel_z = (float)accel_z_temp / 2048.0;

  // Extract temperature readings
  temp_raw = raw_data[3];
  temp = (float)temp_raw / 340.0;

  // Extract gyro readings
  gyro_x_temp = raw_data[4];
  gyro_x = (float)gyro_x_temp / 131.0;
  gyro_y_temp = raw_data[5];
  gyro_y = (float)gyro_y_temp / 131.0;
  gyro_z_temp = raw_data[6];
  gyro_z = (float)gyro_z_temp / 131.0;
}

// Set offset for the readings
void Set_Offset() {
  // Set accel offset
  accel_x += Accelx_offset;
  accel_y += Accely_offset;
  accel_z += Accelz_offset;

  // Set temp offset
  temp += Temp_offset;

  // Set gyro offset
  gyro_x += Gyrox_offset;
  gyro_y += Gyroy_offset;
  gyro_z += Gyroz_offset;
}

// Convert readings to string
void Convert_Readings_To_String() {
  // Convert accel readings
  FloatToStr(accel_x, accel_x_out);
  FloatToStr(accel_y, accel_y_out);
  FloatToStr(accel_z, accel_z_out);

  // Convert temperature readings
  FloatToStr(temp, temp_out);
  
  // Convert gyro readings
  FloatToStr(gyro_x, gyro_x_out);
  FloatToStr(gyro_y, gyro_y_out);
  FloatToStr(gyro_z, gyro_z_out);
}

// Print readings on UART
void Print_Readings() {

  sprintf(tmpTxt, "%f %f %f %f %f %f\r\n", accel_x,accel_y,accel_z,gyro_x,gyro_y,gyro_z);
  UART1_Write_text(tmpTxt);
}

void main() {
  Init_MCU();                    // Initialize MCU
  Init_MPU();                    // Initialize MPU

  while(1) {
    while(INT != 1)              // Wait until INT line becomes 1
      ;
    MPU_I2C_Read_Int(mpu_I2C_ADDR, mpu_rm_ACCEL_XOUT_H, 7, &raw_data);   // Read data from MPU
    Extract_Readings();                                                  // Extract data from the readings
    Set_Offset();                                                        // Set offset
    Convert_Readings_To_String();                                        // Convert readings to strings
    Print_Readings();                                                    // Print readings on UART
  }
}