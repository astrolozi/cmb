
_MPU_I2C_Write:

;MPU_IMU_I2C.c,1 :: 		void MPU_I2C_Write(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
;MPU_IMU_I2C.c,3 :: 		I2C2_Start();                      // issue I2C start signal
	CALL        _I2C2_Start+0, 0
;MPU_IMU_I2C.c,4 :: 		I2C2_Wr(s_addr & 0xFE);            // send byte via I2C  (device address + W(&0xFE))
	MOVLW       254
	ANDWF       FARG_MPU_I2C_Write_s_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,5 :: 		I2C2_Wr(r_addr);                   // send byte (address of EEPROM location)
	MOVF        FARG_MPU_I2C_Write_r_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,6 :: 		for (i = 0 ; i < len ; i++){
	CLRF        MPU_I2C_Write_i_L0+0 
	CLRF        MPU_I2C_Write_i_L0+1 
L_MPU_I2C_Write0:
	MOVLW       0
	SUBWF       MPU_I2C_Write_i_L0+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__MPU_I2C_Write12
	MOVF        FARG_MPU_I2C_Write_len+0, 0 
	SUBWF       MPU_I2C_Write_i_L0+0, 0 
L__MPU_I2C_Write12:
	BTFSC       STATUS+0, 0 
	GOTO        L_MPU_I2C_Write1
;MPU_IMU_I2C.c,7 :: 		I2C2_Wr(*dat++);                 // send data (data to be written)
	MOVFF       FARG_MPU_I2C_Write_dat+0, FSR0
	MOVFF       FARG_MPU_I2C_Write_dat+1, FSR0H
	MOVF        POSTINC0+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
	INFSNZ      FARG_MPU_I2C_Write_dat+0, 1 
	INCF        FARG_MPU_I2C_Write_dat+1, 1 
;MPU_IMU_I2C.c,6 :: 		for (i = 0 ; i < len ; i++){
	INFSNZ      MPU_I2C_Write_i_L0+0, 1 
	INCF        MPU_I2C_Write_i_L0+1, 1 
;MPU_IMU_I2C.c,8 :: 		}
	GOTO        L_MPU_I2C_Write0
L_MPU_I2C_Write1:
;MPU_IMU_I2C.c,9 :: 		I2C2_Stop();                       // issue I2C stop signal
	CALL        _I2C2_Stop+0, 0
;MPU_IMU_I2C.c,10 :: 		}
L_end_MPU_I2C_Write:
	RETURN      0
; end of _MPU_I2C_Write

_MPU_I2C_Read:

;MPU_IMU_I2C.c,12 :: 		void MPU_I2C_Read(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
;MPU_IMU_I2C.c,14 :: 		I2C2_Start();                      // issue I2C start signal
	CALL        _I2C2_Start+0, 0
;MPU_IMU_I2C.c,15 :: 		I2C2_Wr(s_addr & 0xFE);            // send byte via I2C  (device address + W(&0xFE))
	MOVLW       254
	ANDWF       FARG_MPU_I2C_Read_s_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,16 :: 		I2C2_Wr(r_addr);                   // send byte (data address)
	MOVF        FARG_MPU_I2C_Read_r_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,17 :: 		I2C2_Repeated_Start();             // issue I2C signal repeated start
	CALL        _I2C2_Repeated_Start+0, 0
;MPU_IMU_I2C.c,18 :: 		I2C2_Wr(s_addr | 0x01);            // send byte (device address + R(|0x01))
	MOVLW       1
	IORWF       FARG_MPU_I2C_Read_s_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,19 :: 		for (i = 0; i < (len-1); i++){
	CLRF        MPU_I2C_Read_i_L0+0 
	CLRF        MPU_I2C_Read_i_L0+1 
L_MPU_I2C_Read3:
	DECF        FARG_MPU_I2C_Read_len+0, 0 
	MOVWF       R1 
	CLRF        R2 
	MOVLW       0
	SUBWFB      R2, 1 
	MOVF        R2, 0 
	SUBWF       MPU_I2C_Read_i_L0+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__MPU_I2C_Read14
	MOVF        R1, 0 
	SUBWF       MPU_I2C_Read_i_L0+0, 0 
L__MPU_I2C_Read14:
	BTFSC       STATUS+0, 0 
	GOTO        L_MPU_I2C_Read4
;MPU_IMU_I2C.c,20 :: 		*dat++ = I2C2_Rd(_I2C_ACK);      // Read the data (acknowledge)
	MOVLW       1
	MOVWF       FARG_I2C2_Rd_ack+0 
	CALL        _I2C2_Rd+0, 0
	MOVFF       FARG_MPU_I2C_Read_dat+0, FSR1
	MOVFF       FARG_MPU_I2C_Read_dat+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
	INFSNZ      FARG_MPU_I2C_Read_dat+0, 1 
	INCF        FARG_MPU_I2C_Read_dat+1, 1 
;MPU_IMU_I2C.c,19 :: 		for (i = 0; i < (len-1); i++){
	INFSNZ      MPU_I2C_Read_i_L0+0, 1 
	INCF        MPU_I2C_Read_i_L0+1, 1 
;MPU_IMU_I2C.c,21 :: 		}
	GOTO        L_MPU_I2C_Read3
L_MPU_I2C_Read4:
;MPU_IMU_I2C.c,22 :: 		*dat = I2C2_Rd(_I2C_NACK);         // Read the data (NO acknowledge)
	CLRF        FARG_I2C2_Rd_ack+0 
	CALL        _I2C2_Rd+0, 0
	MOVFF       FARG_MPU_I2C_Read_dat+0, FSR1
	MOVFF       FARG_MPU_I2C_Read_dat+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;MPU_IMU_I2C.c,23 :: 		I2C2_Stop();                       // issue I2C stop signal
	CALL        _I2C2_Stop+0, 0
;MPU_IMU_I2C.c,24 :: 		}
L_end_MPU_I2C_Read:
	RETURN      0
; end of _MPU_I2C_Read

_MPU_I2C_Read_Int:

;MPU_IMU_I2C.c,26 :: 		void MPU_I2C_Read_Int(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
;MPU_IMU_I2C.c,29 :: 		I2C2_Start();                      // issue I2C start signal
	CALL        _I2C2_Start+0, 0
;MPU_IMU_I2C.c,30 :: 		I2C2_Wr(s_addr & 0xFE);            // send byte via I2C  (device address + W(&0xFE))
	MOVLW       254
	ANDWF       FARG_MPU_I2C_Read_Int_s_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,31 :: 		I2C2_Wr(r_addr);                   // send byte (data address)
	MOVF        FARG_MPU_I2C_Read_Int_r_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,32 :: 		I2C2_Repeated_Start();             // issue I2C signal repeated start
	CALL        _I2C2_Repeated_Start+0, 0
;MPU_IMU_I2C.c,33 :: 		I2C2_Wr(s_addr | 0x01);            // send byte (device address + R(|0x01))
	MOVLW       1
	IORWF       FARG_MPU_I2C_Read_Int_s_addr+0, 0 
	MOVWF       FARG_I2C2_Wr_data_+0 
	CALL        _I2C2_Wr+0, 0
;MPU_IMU_I2C.c,34 :: 		for (i = 0 ; i < ((len << 1)-1) ; i++){
	CLRF        MPU_I2C_Read_Int_i_L0+0 
	CLRF        MPU_I2C_Read_Int_i_L0+1 
L_MPU_I2C_Read_Int6:
	MOVF        FARG_MPU_I2C_Read_Int_len+0, 0 
	MOVWF       R0 
	MOVLW       0
	MOVWF       R1 
	RLCF        R0, 1 
	BCF         R0, 0 
	RLCF        R1, 1 
	MOVLW       1
	SUBWF       R0, 0 
	MOVWF       R2 
	MOVLW       0
	SUBWFB      R1, 0 
	MOVWF       R3 
	MOVF        R3, 0 
	SUBWF       MPU_I2C_Read_Int_i_L0+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__MPU_I2C_Read_Int16
	MOVF        R2, 0 
	SUBWF       MPU_I2C_Read_Int_i_L0+0, 0 
L__MPU_I2C_Read_Int16:
	BTFSC       STATUS+0, 0 
	GOTO        L_MPU_I2C_Read_Int7
;MPU_IMU_I2C.c,35 :: 		if (i%2) {
	BTFSS       MPU_I2C_Read_Int_i_L0+0, 0 
	GOTO        L_MPU_I2C_Read_Int9
;MPU_IMU_I2C.c,36 :: 		pt = pt - 1;
	MOVLW       1
	SUBWF       MPU_I2C_Read_Int_pt_L0+0, 1 
	MOVLW       0
	SUBWFB      MPU_I2C_Read_Int_pt_L0+1, 1 
;MPU_IMU_I2C.c,37 :: 		}
	GOTO        L_MPU_I2C_Read_Int10
L_MPU_I2C_Read_Int9:
;MPU_IMU_I2C.c,39 :: 		pt = dat + i + 1;
	MOVF        MPU_I2C_Read_Int_i_L0+0, 0 
	ADDWF       FARG_MPU_I2C_Read_Int_dat+0, 0 
	MOVWF       MPU_I2C_Read_Int_pt_L0+0 
	MOVF        MPU_I2C_Read_Int_i_L0+1, 0 
	ADDWFC      FARG_MPU_I2C_Read_Int_dat+1, 0 
	MOVWF       MPU_I2C_Read_Int_pt_L0+1 
	INFSNZ      MPU_I2C_Read_Int_pt_L0+0, 1 
	INCF        MPU_I2C_Read_Int_pt_L0+1, 1 
;MPU_IMU_I2C.c,40 :: 		}
L_MPU_I2C_Read_Int10:
;MPU_IMU_I2C.c,41 :: 		*pt = I2C2_Rd(_I2C_ACK);         // Read the data (acknowledge)
	MOVLW       1
	MOVWF       FARG_I2C2_Rd_ack+0 
	CALL        _I2C2_Rd+0, 0
	MOVFF       MPU_I2C_Read_Int_pt_L0+0, FSR1
	MOVFF       MPU_I2C_Read_Int_pt_L0+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;MPU_IMU_I2C.c,34 :: 		for (i = 0 ; i < ((len << 1)-1) ; i++){
	INFSNZ      MPU_I2C_Read_Int_i_L0+0, 1 
	INCF        MPU_I2C_Read_Int_i_L0+1, 1 
;MPU_IMU_I2C.c,42 :: 		}
	GOTO        L_MPU_I2C_Read_Int6
L_MPU_I2C_Read_Int7:
;MPU_IMU_I2C.c,43 :: 		*(pt - 1) = I2C2_Rd(_I2C_NACK);    // Read the data (NO acknowledge)
	MOVLW       1
	SUBWF       MPU_I2C_Read_Int_pt_L0+0, 0 
	MOVWF       FLOC__MPU_I2C_Read_Int+0 
	MOVLW       0
	SUBWFB      MPU_I2C_Read_Int_pt_L0+1, 0 
	MOVWF       FLOC__MPU_I2C_Read_Int+1 
	CLRF        FARG_I2C2_Rd_ack+0 
	CALL        _I2C2_Rd+0, 0
	MOVFF       FLOC__MPU_I2C_Read_Int+0, FSR1
	MOVFF       FLOC__MPU_I2C_Read_Int+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;MPU_IMU_I2C.c,44 :: 		I2C2_Stop();                       // issue I2C stop signal
	CALL        _I2C2_Stop+0, 0
;MPU_IMU_I2C.c,45 :: 		}
L_end_MPU_I2C_Read_Int:
	RETURN      0
; end of _MPU_I2C_Read_Int
