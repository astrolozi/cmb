#line 1 "F:/Documents/cmb/Automer/clicker 2 for PIC18FJ/MPU_IMU_I2C.c"
void MPU_I2C_Write(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
 unsigned int i;
 I2C2_Start();
 I2C2_Wr(s_addr & 0xFE);
 I2C2_Wr(r_addr);
 for (i = 0 ; i < len ; i++){
 I2C2_Wr(*dat++);
 }
 I2C2_Stop();
}

void MPU_I2C_Read(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
 unsigned int i;
 I2C2_Start();
 I2C2_Wr(s_addr & 0xFE);
 I2C2_Wr(r_addr);
 I2C2_Repeated_Start();
 I2C2_Wr(s_addr | 0x01);
 for (i = 0; i < (len-1); i++){
 *dat++ = I2C2_Rd(_I2C_ACK);
 }
 *dat = I2C2_Rd(_I2C_NACK);
 I2C2_Stop();
}

void MPU_I2C_Read_Int(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
 unsigned int i;
 unsigned short *pt;
 I2C2_Start();
 I2C2_Wr(s_addr & 0xFE);
 I2C2_Wr(r_addr);
 I2C2_Repeated_Start();
 I2C2_Wr(s_addr | 0x01);
 for (i = 0 ; i < ((len << 1)-1) ; i++){
 if (i%2) {
 pt = pt - 1;
 }
 else {
 pt = dat + i + 1;
 }
 *pt = I2C2_Rd(_I2C_ACK);
 }
 *(pt - 1) = I2C2_Rd(_I2C_NACK);
 I2C2_Stop();
}
