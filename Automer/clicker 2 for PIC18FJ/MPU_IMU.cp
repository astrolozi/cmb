#line 1 "F:/Documents/cmb/Automer/clicker 2 for PIC18FJ/MPU_IMU.c"
#line 1 "f:/documents/cmb/automer/clicker 2 for pic18fj/mpu_imu_register_map.h"
#line 1 "f:/documents/cmb/automer/clicker 2 for pic18fj/mpu_imu_i2c.h"
void MPU_I2C_Write(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat);
void MPU_I2C_Read_Int(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat);
void MPU_I2C_Read(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat);
#line 43 "F:/Documents/cmb/Automer/clicker 2 for PIC18FJ/MPU_IMU.c"
sbit INT at RB3_bit;
sbit INT_Direction at TRISB3_bit;

char* tmpTxt[30];

unsigned int raw_data[7];
int gyro_x_temp, gyro_y_temp, gyro_z_temp, accel_x_temp, accel_y_temp, accel_z_temp, temp_raw;
char i, buff, gyro_x_out[15], gyro_y_out[15], gyro_z_out[15], accel_x_out[15], accel_y_out[15], accel_z_out[15], temp_out[15];
float temp, gyro_x, gyro_y, gyro_z, accel_x, accel_y, accel_z;


void Init_MPU(){
 MPU_I2C_Write( 0xD2 ,  0x6B  , 1, 0x80);
 Delay_ms(100);
 MPU_I2C_Write( 0xD2 ,  0x6B  , 1, 0x00);
 Delay_ms(100);
 MPU_I2C_Write( 0xD2 ,  0x23  , 1, 0x78);
 Delay_ms(100);
 MPU_I2C_Write( 0xD2 ,  0x38  , 1, 0x10);
 Delay_ms(100);
 MPU_I2C_Read ( 0xD2 ,  0x1C , 1, &raw_data);
 Delay_ms(100);
 raw_data[0] |= 0x18;
 MPU_I2C_Write( 0xD2 ,  0x1C , 1, &raw_data[0]);
 Delay_ms(100);
 MPU_I2C_Read ( 0xD2 ,  0x38 , 1, &raw_data);
 Delay_ms(100);
 raw_data[0] |= 0x11;
 MPU_I2C_Write( 0xD2 ,  0x38 , 1, &raw_data[0]);
 Delay_ms(100);
}


void Init_MCU(){

 ANCON0 = 0xFF;
 ANCON1 = 0xFF;


 TRISC = 0;


 INT_Direction = 1;


 I2C2_Init(400000);
 Delay_ms(100);


 UART1_Init(115200);
 Delay_ms(100);
}


void Extract_Readings() {

 accel_x_temp = raw_data[0];
 accel_x = (float)accel_x_temp / 2048.0;
 accel_y_temp = raw_data[1];
 accel_y = (float)accel_y_temp / 2048.0;
 accel_z_temp = raw_data[2];
 accel_z = (float)accel_z_temp / 2048.0;


 temp_raw = raw_data[3];
 temp = (float)temp_raw / 340.0;


 gyro_x_temp = raw_data[4];
 gyro_x = (float)gyro_x_temp / 131.0;
 gyro_y_temp = raw_data[5];
 gyro_y = (float)gyro_y_temp / 131.0;
 gyro_z_temp = raw_data[6];
 gyro_z = (float)gyro_z_temp / 131.0;
}


void Set_Offset() {

 accel_x +=  -0.07 ;
 accel_y +=  0.02 ;
 accel_z +=  0.2 ;


 temp +=  36.53 ;


 gyro_x +=  3.3 ;
 gyro_y +=  0 ;
 gyro_z +=  0 ;
}


void Convert_Readings_To_String() {

 FloatToStr(accel_x, accel_x_out);
 FloatToStr(accel_y, accel_y_out);
 FloatToStr(accel_z, accel_z_out);


 FloatToStr(temp, temp_out);


 FloatToStr(gyro_x, gyro_x_out);
 FloatToStr(gyro_y, gyro_y_out);
 FloatToStr(gyro_z, gyro_z_out);
}


void Print_Readings() {

 sprintf(tmpTxt, "%f %f %f %f %f %f\r\n", accel_x,accel_y,accel_z,gyro_x,gyro_y,gyro_z);
 UART1_Write_text(tmpTxt);
}

void main() {
 Init_MCU();
 Init_MPU();

 while(1) {
 while(INT != 1)
 ;
 MPU_I2C_Read_Int( 0xD2 ,  0x3B , 7, &raw_data);
 Extract_Readings();
 Set_Offset();
 Convert_Readings_To_String();
 Print_Readings();
 }
}
