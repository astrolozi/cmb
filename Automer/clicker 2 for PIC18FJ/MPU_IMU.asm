
_Init_MPU:

;MPU_IMU.c,54 :: 		void Init_MPU(){
;MPU_IMU.c,55 :: 		MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_PWR_MGMT_1 , 1, 0x80);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Write_s_addr+0 
	MOVLW       107
	MOVWF       FARG_MPU_I2C_Write_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Write_len+0 
	MOVLW       128
	MOVWF       FARG_MPU_I2C_Write_dat+0 
	MOVLW       0
	MOVWF       FARG_MPU_I2C_Write_dat+1 
	CALL        _MPU_I2C_Write+0, 0
;MPU_IMU.c,56 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU0:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU0
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU0
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU0
	NOP
;MPU_IMU.c,57 :: 		MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_PWR_MGMT_1 , 1, 0x00);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Write_s_addr+0 
	MOVLW       107
	MOVWF       FARG_MPU_I2C_Write_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Write_len+0 
	CLRF        FARG_MPU_I2C_Write_dat+0 
	CLRF        FARG_MPU_I2C_Write_dat+1 
	CALL        _MPU_I2C_Write+0, 0
;MPU_IMU.c,58 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU1:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU1
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU1
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU1
	NOP
;MPU_IMU.c,59 :: 		MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_FIFO_EN , 1, 0x78);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Write_s_addr+0 
	MOVLW       35
	MOVWF       FARG_MPU_I2C_Write_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Write_len+0 
	MOVLW       120
	MOVWF       FARG_MPU_I2C_Write_dat+0 
	MOVLW       0
	MOVWF       FARG_MPU_I2C_Write_dat+1 
	CALL        _MPU_I2C_Write+0, 0
;MPU_IMU.c,60 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU2:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU2
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU2
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU2
	NOP
;MPU_IMU.c,61 :: 		MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_INT_ENABLE , 1, 0x10);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Write_s_addr+0 
	MOVLW       56
	MOVWF       FARG_MPU_I2C_Write_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Write_len+0 
	MOVLW       16
	MOVWF       FARG_MPU_I2C_Write_dat+0 
	MOVLW       0
	MOVWF       FARG_MPU_I2C_Write_dat+1 
	CALL        _MPU_I2C_Write+0, 0
;MPU_IMU.c,62 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU3:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU3
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU3
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU3
	NOP
;MPU_IMU.c,63 :: 		MPU_I2C_Read (mpu_I2C_ADDR, mpu_rm_ACCEL_CONFIG, 1, &raw_data);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Read_s_addr+0 
	MOVLW       28
	MOVWF       FARG_MPU_I2C_Read_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Read_len+0 
	MOVLW       _raw_data+0
	MOVWF       FARG_MPU_I2C_Read_dat+0 
	MOVLW       hi_addr(_raw_data+0)
	MOVWF       FARG_MPU_I2C_Read_dat+1 
	CALL        _MPU_I2C_Read+0, 0
;MPU_IMU.c,64 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU4:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU4
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU4
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU4
	NOP
;MPU_IMU.c,65 :: 		raw_data[0] |= 0x18;
	MOVLW       24
	IORWF       _raw_data+0, 1 
	MOVLW       0
	IORWF       _raw_data+1, 1 
;MPU_IMU.c,66 :: 		MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_ACCEL_CONFIG, 1, &raw_data[0]);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Write_s_addr+0 
	MOVLW       28
	MOVWF       FARG_MPU_I2C_Write_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Write_len+0 
	MOVLW       _raw_data+0
	MOVWF       FARG_MPU_I2C_Write_dat+0 
	MOVLW       hi_addr(_raw_data+0)
	MOVWF       FARG_MPU_I2C_Write_dat+1 
	CALL        _MPU_I2C_Write+0, 0
;MPU_IMU.c,67 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU5:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU5
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU5
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU5
	NOP
;MPU_IMU.c,68 :: 		MPU_I2C_Read (mpu_I2C_ADDR, mpu_rm_INT_ENABLE, 1, &raw_data);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Read_s_addr+0 
	MOVLW       56
	MOVWF       FARG_MPU_I2C_Read_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Read_len+0 
	MOVLW       _raw_data+0
	MOVWF       FARG_MPU_I2C_Read_dat+0 
	MOVLW       hi_addr(_raw_data+0)
	MOVWF       FARG_MPU_I2C_Read_dat+1 
	CALL        _MPU_I2C_Read+0, 0
;MPU_IMU.c,69 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU6:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU6
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU6
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU6
	NOP
;MPU_IMU.c,70 :: 		raw_data[0] |= 0x11;
	MOVLW       17
	IORWF       _raw_data+0, 1 
	MOVLW       0
	IORWF       _raw_data+1, 1 
;MPU_IMU.c,71 :: 		MPU_I2C_Write(mpu_I2C_ADDR, mpu_rm_INT_ENABLE, 1, &raw_data[0]);
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Write_s_addr+0 
	MOVLW       56
	MOVWF       FARG_MPU_I2C_Write_r_addr+0 
	MOVLW       1
	MOVWF       FARG_MPU_I2C_Write_len+0 
	MOVLW       _raw_data+0
	MOVWF       FARG_MPU_I2C_Write_dat+0 
	MOVLW       hi_addr(_raw_data+0)
	MOVWF       FARG_MPU_I2C_Write_dat+1 
	CALL        _MPU_I2C_Write+0, 0
;MPU_IMU.c,72 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MPU7:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MPU7
	DECFSZ      R12, 1, 1
	BRA         L_Init_MPU7
	DECFSZ      R11, 1, 1
	BRA         L_Init_MPU7
	NOP
;MPU_IMU.c,73 :: 		}
L_end_Init_MPU:
	RETURN      0
; end of _Init_MPU

_Init_MCU:

;MPU_IMU.c,76 :: 		void Init_MCU(){
;MPU_IMU.c,78 :: 		ANCON0 = 0xFF;            // All pins to digital
	MOVLW       255
	MOVWF       ANCON0+0 
;MPU_IMU.c,79 :: 		ANCON1 = 0xFF;
	MOVLW       255
	MOVWF       ANCON1+0 
;MPU_IMU.c,82 :: 		TRISC = 0;
	CLRF        TRISC+0 
;MPU_IMU.c,85 :: 		INT_Direction = 1;
	BSF         TRISB3_bit+0, BitPos(TRISB3_bit+0) 
;MPU_IMU.c,88 :: 		I2C2_Init(400000);
	MOVLW       30
	MOVWF       SSP2ADD+0 
	CALL        _I2C2_Init+0, 0
;MPU_IMU.c,89 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MCU8:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MCU8
	DECFSZ      R12, 1, 1
	BRA         L_Init_MCU8
	DECFSZ      R11, 1, 1
	BRA         L_Init_MCU8
	NOP
;MPU_IMU.c,92 :: 		UART1_Init(115200);
	BSF         BAUDCON+0, 3, 0
	CLRF        SPBRGH+0 
	MOVLW       103
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;MPU_IMU.c,93 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_Init_MCU9:
	DECFSZ      R13, 1, 1
	BRA         L_Init_MCU9
	DECFSZ      R12, 1, 1
	BRA         L_Init_MCU9
	DECFSZ      R11, 1, 1
	BRA         L_Init_MCU9
	NOP
;MPU_IMU.c,94 :: 		}
L_end_Init_MCU:
	RETURN      0
; end of _Init_MCU

_Extract_Readings:

;MPU_IMU.c,97 :: 		void Extract_Readings() {
;MPU_IMU.c,99 :: 		accel_x_temp = raw_data[0];
	MOVF        _raw_data+0, 0 
	MOVWF       _accel_x_temp+0 
	MOVF        _raw_data+1, 0 
	MOVWF       _accel_x_temp+1 
;MPU_IMU.c,100 :: 		accel_x = (float)accel_x_temp / 2048.0;
	MOVF        _raw_data+0, 0 
	MOVWF       R0 
	MOVF        _raw_data+1, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       0
	MOVWF       R6 
	MOVLW       138
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _accel_x+0 
	MOVF        R1, 0 
	MOVWF       _accel_x+1 
	MOVF        R2, 0 
	MOVWF       _accel_x+2 
	MOVF        R3, 0 
	MOVWF       _accel_x+3 
;MPU_IMU.c,101 :: 		accel_y_temp = raw_data[1];
	MOVF        _raw_data+2, 0 
	MOVWF       _accel_y_temp+0 
	MOVF        _raw_data+3, 0 
	MOVWF       _accel_y_temp+1 
;MPU_IMU.c,102 :: 		accel_y = (float)accel_y_temp / 2048.0;
	MOVF        _raw_data+2, 0 
	MOVWF       R0 
	MOVF        _raw_data+3, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       0
	MOVWF       R6 
	MOVLW       138
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _accel_y+0 
	MOVF        R1, 0 
	MOVWF       _accel_y+1 
	MOVF        R2, 0 
	MOVWF       _accel_y+2 
	MOVF        R3, 0 
	MOVWF       _accel_y+3 
;MPU_IMU.c,103 :: 		accel_z_temp = raw_data[2];
	MOVF        _raw_data+4, 0 
	MOVWF       _accel_z_temp+0 
	MOVF        _raw_data+5, 0 
	MOVWF       _accel_z_temp+1 
;MPU_IMU.c,104 :: 		accel_z = (float)accel_z_temp / 2048.0;
	MOVF        _raw_data+4, 0 
	MOVWF       R0 
	MOVF        _raw_data+5, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       0
	MOVWF       R6 
	MOVLW       138
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _accel_z+0 
	MOVF        R1, 0 
	MOVWF       _accel_z+1 
	MOVF        R2, 0 
	MOVWF       _accel_z+2 
	MOVF        R3, 0 
	MOVWF       _accel_z+3 
;MPU_IMU.c,107 :: 		temp_raw = raw_data[3];
	MOVF        _raw_data+6, 0 
	MOVWF       _temp_raw+0 
	MOVF        _raw_data+7, 0 
	MOVWF       _temp_raw+1 
;MPU_IMU.c,108 :: 		temp = (float)temp_raw / 340.0;
	MOVF        _raw_data+6, 0 
	MOVWF       R0 
	MOVF        _raw_data+7, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       42
	MOVWF       R6 
	MOVLW       135
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _temp+0 
	MOVF        R1, 0 
	MOVWF       _temp+1 
	MOVF        R2, 0 
	MOVWF       _temp+2 
	MOVF        R3, 0 
	MOVWF       _temp+3 
;MPU_IMU.c,111 :: 		gyro_x_temp = raw_data[4];
	MOVF        _raw_data+8, 0 
	MOVWF       _gyro_x_temp+0 
	MOVF        _raw_data+9, 0 
	MOVWF       _gyro_x_temp+1 
;MPU_IMU.c,112 :: 		gyro_x = (float)gyro_x_temp / 131.0;
	MOVF        _raw_data+8, 0 
	MOVWF       R0 
	MOVF        _raw_data+9, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       3
	MOVWF       R6 
	MOVLW       134
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _gyro_x+0 
	MOVF        R1, 0 
	MOVWF       _gyro_x+1 
	MOVF        R2, 0 
	MOVWF       _gyro_x+2 
	MOVF        R3, 0 
	MOVWF       _gyro_x+3 
;MPU_IMU.c,113 :: 		gyro_y_temp = raw_data[5];
	MOVF        _raw_data+10, 0 
	MOVWF       _gyro_y_temp+0 
	MOVF        _raw_data+11, 0 
	MOVWF       _gyro_y_temp+1 
;MPU_IMU.c,114 :: 		gyro_y = (float)gyro_y_temp / 131.0;
	MOVF        _raw_data+10, 0 
	MOVWF       R0 
	MOVF        _raw_data+11, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       3
	MOVWF       R6 
	MOVLW       134
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _gyro_y+0 
	MOVF        R1, 0 
	MOVWF       _gyro_y+1 
	MOVF        R2, 0 
	MOVWF       _gyro_y+2 
	MOVF        R3, 0 
	MOVWF       _gyro_y+3 
;MPU_IMU.c,115 :: 		gyro_z_temp = raw_data[6];
	MOVF        _raw_data+12, 0 
	MOVWF       _gyro_z_temp+0 
	MOVF        _raw_data+13, 0 
	MOVWF       _gyro_z_temp+1 
;MPU_IMU.c,116 :: 		gyro_z = (float)gyro_z_temp / 131.0;
	MOVF        _raw_data+12, 0 
	MOVWF       R0 
	MOVF        _raw_data+13, 0 
	MOVWF       R1 
	CALL        _int2double+0, 0
	MOVLW       0
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVLW       3
	MOVWF       R6 
	MOVLW       134
	MOVWF       R7 
	CALL        _Div_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _gyro_z+0 
	MOVF        R1, 0 
	MOVWF       _gyro_z+1 
	MOVF        R2, 0 
	MOVWF       _gyro_z+2 
	MOVF        R3, 0 
	MOVWF       _gyro_z+3 
;MPU_IMU.c,117 :: 		}
L_end_Extract_Readings:
	RETURN      0
; end of _Extract_Readings

_Set_Offset:

;MPU_IMU.c,120 :: 		void Set_Offset() {
;MPU_IMU.c,122 :: 		accel_x += Accelx_offset;
	MOVF        _accel_x+0, 0 
	MOVWF       R0 
	MOVF        _accel_x+1, 0 
	MOVWF       R1 
	MOVF        _accel_x+2, 0 
	MOVWF       R2 
	MOVF        _accel_x+3, 0 
	MOVWF       R3 
	MOVLW       41
	MOVWF       R4 
	MOVLW       92
	MOVWF       R5 
	MOVLW       143
	MOVWF       R6 
	MOVLW       123
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _accel_x+0 
	MOVF        R1, 0 
	MOVWF       _accel_x+1 
	MOVF        R2, 0 
	MOVWF       _accel_x+2 
	MOVF        R3, 0 
	MOVWF       _accel_x+3 
;MPU_IMU.c,123 :: 		accel_y += Accely_offset;
	MOVF        _accel_y+0, 0 
	MOVWF       R0 
	MOVF        _accel_y+1, 0 
	MOVWF       R1 
	MOVF        _accel_y+2, 0 
	MOVWF       R2 
	MOVF        _accel_y+3, 0 
	MOVWF       R3 
	MOVLW       10
	MOVWF       R4 
	MOVLW       215
	MOVWF       R5 
	MOVLW       35
	MOVWF       R6 
	MOVLW       121
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _accel_y+0 
	MOVF        R1, 0 
	MOVWF       _accel_y+1 
	MOVF        R2, 0 
	MOVWF       _accel_y+2 
	MOVF        R3, 0 
	MOVWF       _accel_y+3 
;MPU_IMU.c,124 :: 		accel_z += Accelz_offset;
	MOVF        _accel_z+0, 0 
	MOVWF       R0 
	MOVF        _accel_z+1, 0 
	MOVWF       R1 
	MOVF        _accel_z+2, 0 
	MOVWF       R2 
	MOVF        _accel_z+3, 0 
	MOVWF       R3 
	MOVLW       205
	MOVWF       R4 
	MOVLW       204
	MOVWF       R5 
	MOVLW       76
	MOVWF       R6 
	MOVLW       124
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _accel_z+0 
	MOVF        R1, 0 
	MOVWF       _accel_z+1 
	MOVF        R2, 0 
	MOVWF       _accel_z+2 
	MOVF        R3, 0 
	MOVWF       _accel_z+3 
;MPU_IMU.c,127 :: 		temp += Temp_offset;
	MOVF        _temp+0, 0 
	MOVWF       R0 
	MOVF        _temp+1, 0 
	MOVWF       R1 
	MOVF        _temp+2, 0 
	MOVWF       R2 
	MOVF        _temp+3, 0 
	MOVWF       R3 
	MOVLW       184
	MOVWF       R4 
	MOVLW       30
	MOVWF       R5 
	MOVLW       18
	MOVWF       R6 
	MOVLW       132
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _temp+0 
	MOVF        R1, 0 
	MOVWF       _temp+1 
	MOVF        R2, 0 
	MOVWF       _temp+2 
	MOVF        R3, 0 
	MOVWF       _temp+3 
;MPU_IMU.c,130 :: 		gyro_x += Gyrox_offset;
	MOVF        _gyro_x+0, 0 
	MOVWF       R0 
	MOVF        _gyro_x+1, 0 
	MOVWF       R1 
	MOVF        _gyro_x+2, 0 
	MOVWF       R2 
	MOVF        _gyro_x+3, 0 
	MOVWF       R3 
	MOVLW       51
	MOVWF       R4 
	MOVLW       51
	MOVWF       R5 
	MOVLW       83
	MOVWF       R6 
	MOVLW       128
	MOVWF       R7 
	CALL        _Add_32x32_FP+0, 0
	MOVF        R0, 0 
	MOVWF       _gyro_x+0 
	MOVF        R1, 0 
	MOVWF       _gyro_x+1 
	MOVF        R2, 0 
	MOVWF       _gyro_x+2 
	MOVF        R3, 0 
	MOVWF       _gyro_x+3 
;MPU_IMU.c,131 :: 		gyro_y += Gyroy_offset;
;MPU_IMU.c,132 :: 		gyro_z += Gyroz_offset;
;MPU_IMU.c,133 :: 		}
L_end_Set_Offset:
	RETURN      0
; end of _Set_Offset

_Convert_Readings_To_String:

;MPU_IMU.c,136 :: 		void Convert_Readings_To_String() {
;MPU_IMU.c,138 :: 		FloatToStr(accel_x, accel_x_out);
	MOVF        _accel_x+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _accel_x+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _accel_x+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _accel_x+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _accel_x_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_accel_x_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,139 :: 		FloatToStr(accel_y, accel_y_out);
	MOVF        _accel_y+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _accel_y+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _accel_y+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _accel_y+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _accel_y_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_accel_y_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,140 :: 		FloatToStr(accel_z, accel_z_out);
	MOVF        _accel_z+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _accel_z+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _accel_z+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _accel_z+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _accel_z_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_accel_z_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,143 :: 		FloatToStr(temp, temp_out);
	MOVF        _temp+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _temp+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _temp+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _temp+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _temp_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_temp_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,146 :: 		FloatToStr(gyro_x, gyro_x_out);
	MOVF        _gyro_x+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _gyro_x+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _gyro_x+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _gyro_x+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _gyro_x_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_gyro_x_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,147 :: 		FloatToStr(gyro_y, gyro_y_out);
	MOVF        _gyro_y+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _gyro_y+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _gyro_y+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _gyro_y+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _gyro_y_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_gyro_y_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,148 :: 		FloatToStr(gyro_z, gyro_z_out);
	MOVF        _gyro_z+0, 0 
	MOVWF       FARG_FloatToStr_fnum+0 
	MOVF        _gyro_z+1, 0 
	MOVWF       FARG_FloatToStr_fnum+1 
	MOVF        _gyro_z+2, 0 
	MOVWF       FARG_FloatToStr_fnum+2 
	MOVF        _gyro_z+3, 0 
	MOVWF       FARG_FloatToStr_fnum+3 
	MOVLW       _gyro_z_out+0
	MOVWF       FARG_FloatToStr_str+0 
	MOVLW       hi_addr(_gyro_z_out+0)
	MOVWF       FARG_FloatToStr_str+1 
	CALL        _FloatToStr+0, 0
;MPU_IMU.c,149 :: 		}
L_end_Convert_Readings_To_String:
	RETURN      0
; end of _Convert_Readings_To_String

_Print_Readings:

;MPU_IMU.c,152 :: 		void Print_Readings() {
;MPU_IMU.c,154 :: 		sprintf(tmpTxt, "%f %f %f %f %f %f\r\n", accel_x,accel_y,accel_z,gyro_x,gyro_y,gyro_z);
	MOVLW       _tmpTxt+0
	MOVWF       FARG_sprintf_wh+0 
	MOVLW       hi_addr(_tmpTxt+0)
	MOVWF       FARG_sprintf_wh+1 
	MOVLW       ?lstr_1_MPU_IMU+0
	MOVWF       FARG_sprintf_f+0 
	MOVLW       hi_addr(?lstr_1_MPU_IMU+0)
	MOVWF       FARG_sprintf_f+1 
	MOVLW       higher_addr(?lstr_1_MPU_IMU+0)
	MOVWF       FARG_sprintf_f+2 
	MOVF        _accel_x+0, 0 
	MOVWF       FARG_sprintf_wh+5 
	MOVF        _accel_x+1, 0 
	MOVWF       FARG_sprintf_wh+6 
	MOVF        _accel_x+2, 0 
	MOVWF       FARG_sprintf_wh+7 
	MOVF        _accel_x+3, 0 
	MOVWF       FARG_sprintf_wh+8 
	MOVF        _accel_y+0, 0 
	MOVWF       FARG_sprintf_wh+9 
	MOVF        _accel_y+1, 0 
	MOVWF       FARG_sprintf_wh+10 
	MOVF        _accel_y+2, 0 
	MOVWF       FARG_sprintf_wh+11 
	MOVF        _accel_y+3, 0 
	MOVWF       FARG_sprintf_wh+12 
	MOVF        _accel_z+0, 0 
	MOVWF       FARG_sprintf_wh+13 
	MOVF        _accel_z+1, 0 
	MOVWF       FARG_sprintf_wh+14 
	MOVF        _accel_z+2, 0 
	MOVWF       FARG_sprintf_wh+15 
	MOVF        _accel_z+3, 0 
	MOVWF       FARG_sprintf_wh+16 
	MOVF        _gyro_x+0, 0 
	MOVWF       FARG_sprintf_wh+17 
	MOVF        _gyro_x+1, 0 
	MOVWF       FARG_sprintf_wh+18 
	MOVF        _gyro_x+2, 0 
	MOVWF       FARG_sprintf_wh+19 
	MOVF        _gyro_x+3, 0 
	MOVWF       FARG_sprintf_wh+20 
	MOVF        _gyro_y+0, 0 
	MOVWF       FARG_sprintf_wh+21 
	MOVF        _gyro_y+1, 0 
	MOVWF       FARG_sprintf_wh+22 
	MOVF        _gyro_y+2, 0 
	MOVWF       FARG_sprintf_wh+23 
	MOVF        _gyro_y+3, 0 
	MOVWF       FARG_sprintf_wh+24 
	MOVF        _gyro_z+0, 0 
	MOVWF       FARG_sprintf_wh+25 
	MOVF        _gyro_z+1, 0 
	MOVWF       FARG_sprintf_wh+26 
	MOVF        _gyro_z+2, 0 
	MOVWF       FARG_sprintf_wh+27 
	MOVF        _gyro_z+3, 0 
	MOVWF       FARG_sprintf_wh+28 
	CALL        _sprintf+0, 0
;MPU_IMU.c,155 :: 		UART1_Write_text(tmpTxt);
	MOVLW       _tmpTxt+0
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVLW       hi_addr(_tmpTxt+0)
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;MPU_IMU.c,156 :: 		}
L_end_Print_Readings:
	RETURN      0
; end of _Print_Readings

_main:

;MPU_IMU.c,158 :: 		void main() {
;MPU_IMU.c,159 :: 		Init_MCU();                    // Initialize MCU
	CALL        _Init_MCU+0, 0
;MPU_IMU.c,160 :: 		Init_MPU();                    // Initialize MPU
	CALL        _Init_MPU+0, 0
;MPU_IMU.c,162 :: 		while(1) {
L_main10:
;MPU_IMU.c,163 :: 		while(INT != 1)              // Wait until INT line becomes 1
L_main12:
	BTFSC       RB3_bit+0, BitPos(RB3_bit+0) 
	GOTO        L_main13
;MPU_IMU.c,164 :: 		;
	GOTO        L_main12
L_main13:
;MPU_IMU.c,165 :: 		MPU_I2C_Read_Int(mpu_I2C_ADDR, mpu_rm_ACCEL_XOUT_H, 7, &raw_data);   // Read data from MPU
	MOVLW       210
	MOVWF       FARG_MPU_I2C_Read_Int_s_addr+0 
	MOVLW       59
	MOVWF       FARG_MPU_I2C_Read_Int_r_addr+0 
	MOVLW       7
	MOVWF       FARG_MPU_I2C_Read_Int_len+0 
	MOVLW       _raw_data+0
	MOVWF       FARG_MPU_I2C_Read_Int_dat+0 
	MOVLW       hi_addr(_raw_data+0)
	MOVWF       FARG_MPU_I2C_Read_Int_dat+1 
	CALL        _MPU_I2C_Read_Int+0, 0
;MPU_IMU.c,166 :: 		Extract_Readings();                                                  // Extract data from the readings
	CALL        _Extract_Readings+0, 0
;MPU_IMU.c,167 :: 		Set_Offset();                                                        // Set offset
	CALL        _Set_Offset+0, 0
;MPU_IMU.c,168 :: 		Convert_Readings_To_String();                                        // Convert readings to strings
	CALL        _Convert_Readings_To_String+0, 0
;MPU_IMU.c,169 :: 		Print_Readings();                                                    // Print readings on UART
	CALL        _Print_Readings+0, 0
;MPU_IMU.c,170 :: 		}
	GOTO        L_main10
;MPU_IMU.c,171 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
