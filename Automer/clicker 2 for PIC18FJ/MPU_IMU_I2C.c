void MPU_I2C_Write(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
  unsigned int i;
  I2C2_Start();                      // issue I2C start signal
  I2C2_Wr(s_addr & 0xFE);            // send byte via I2C  (device address + W(&0xFE))
  I2C2_Wr(r_addr);                   // send byte (address of EEPROM location)
  for (i = 0 ; i < len ; i++){
    I2C2_Wr(*dat++);                 // send data (data to be written)
  }
  I2C2_Stop();                       // issue I2C stop signal
}

void MPU_I2C_Read(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
  unsigned int i;
  I2C2_Start();                      // issue I2C start signal
  I2C2_Wr(s_addr & 0xFE);            // send byte via I2C  (device address + W(&0xFE))
  I2C2_Wr(r_addr);                   // send byte (data address)
  I2C2_Repeated_Start();             // issue I2C signal repeated start
  I2C2_Wr(s_addr | 0x01);            // send byte (device address + R(|0x01))
  for (i = 0; i < (len-1); i++){
    *dat++ = I2C2_Rd(_I2C_ACK);      // Read the data (acknowledge)
  }
  *dat = I2C2_Rd(_I2C_NACK);         // Read the data (NO acknowledge)
  I2C2_Stop();                       // issue I2C stop signal
}

void MPU_I2C_Read_Int(unsigned char s_addr, unsigned char r_addr, unsigned char len, unsigned char *dat) {
  unsigned int i;
  unsigned short *pt;
  I2C2_Start();                      // issue I2C start signal
  I2C2_Wr(s_addr & 0xFE);            // send byte via I2C  (device address + W(&0xFE))
  I2C2_Wr(r_addr);                   // send byte (data address)
  I2C2_Repeated_Start();             // issue I2C signal repeated start
  I2C2_Wr(s_addr | 0x01);            // send byte (device address + R(|0x01))
  for (i = 0 ; i < ((len << 1)-1) ; i++){
    if (i%2) {
      pt = pt - 1;
    }
    else {
      pt = dat + i + 1;
    }
    *pt = I2C2_Rd(_I2C_ACK);         // Read the data (acknowledge)
  }
  *(pt - 1) = I2C2_Rd(_I2C_NACK);    // Read the data (NO acknowledge)
  I2C2_Stop();                       // issue I2C stop signal
}