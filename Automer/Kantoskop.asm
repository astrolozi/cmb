_main:
;Kantoskop.c,33 :: 		void main() {
;Kantoskop.c,34 :: 		CHECON = 30;
ORI	R2, R0, 30
SW	R2, Offset(CHECON+0)(GP)
;Kantoskop.c,35 :: 		AD1PCFG = 0xFFFF;
ORI	R2, R0, 65535
SW	R2, Offset(AD1PCFG+0)(GP)
;Kantoskop.c,36 :: 		AD1PCFG = 0xFFFF;            // Configure AN pins as digital I/O
ORI	R2, R0, 65535
SW	R2, Offset(AD1PCFG+0)(GP)
;Kantoskop.c,37 :: 		JTAGEN_bit = 0;              // Disable JTAG
_LX	
INS	R2, R0, BitPos(JTAGEN_bit+0), 1
_SX	
;Kantoskop.c,38 :: 		UART2_Init(256000);              // Initialize UART module at 9600 bps
LUI	R25, 3
ORI	R25, R25, 59392
JAL	_UART2_Init+0
NOP	
;Kantoskop.c,39 :: 		Delay_ms(100);                  // Wait for UART module to stabilize
LUI	R24, 40
ORI	R24, R24, 45226
L_main0:
ADDIU	R24, R24, -1
BNE	R24, R0, L_main0
NOP	
;Kantoskop.c,41 :: 		SCL_Direction = 0;                 // SCL is output
LUI	R2, BitMask(TRISA2_bit+0)
ORI	R2, R2, BitMask(TRISA2_bit+0)
_SX	
;Kantoskop.c,42 :: 		SDA_Out_Pin = 1;                   // SDA is high
LUI	R2, BitMask(LATA3_bit+0)
ORI	R2, R2, BitMask(LATA3_bit+0)
_SX	
;Kantoskop.c,44 :: 		MCP3551_Init();
JAL	_MCP3551_Init+0
NOP	
;Kantoskop.c,45 :: 		Read_SHT11(&SHTtemperature, &SHTrel_humidity);
LUI	R26, hi_addr(_SHTrel_humidity+0)
ORI	R26, R26, lo_addr(_SHTrel_humidity+0)
LUI	R25, hi_addr(_SHTtemperature+0)
ORI	R25, R25, lo_addr(_SHTtemperature+0)
JAL	_Read_SHT11+0
NOP	
;Kantoskop.c,47 :: 		TRISA1_bit = 0;
LUI	R2, BitMask(TRISA1_bit+0)
ORI	R2, R2, BitMask(TRISA1_bit+0)
_SX	
;Kantoskop.c,48 :: 		LATA1_bit = 0;
LUI	R2, BitMask(LATA1_bit+0)
ORI	R2, R2, BitMask(LATA1_bit+0)
_SX	
;Kantoskop.c,49 :: 		while(1){
L_main2:
;Kantoskop.c,50 :: 		voltage = (MCP3551_Read() / 4194304.0) * 6.6;
JAL	_MCP3551_Read+0
NOP	
MOVZ	R4, R2, R0
JAL	__SignedIntegralToFLoat+0
NOP	
LUI	R6, 19072
ORI	R6, R6, 0
MOVZ	R4, R2, R0
JAL	__Div_FP+0
NOP	
LUI	R4, 16595
ORI	R4, R4, 13107
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
SW	R2, Offset(_voltage+0)(GP)
;Kantoskop.c,52 :: 		while(UART2_Data_Ready()){
L_main4:
JAL	_UART2_Data_Ready+0
NOP	
BNE	R2, R0, L__main10
NOP	
J	L_main5
NOP	
L__main10:
;Kantoskop.c,53 :: 		tmp = UART2_Read();
JAL	_UART2_Read+0
NOP	
SB	R2, Offset(_tmp+0)(GP)
;Kantoskop.c,54 :: 		if(tmp == 'R'){
ANDI	R3, R2, 255
ORI	R2, R0, 82
BEQ	R3, R2, L__main11
NOP	
J	L_main6
NOP	
L__main11:
;Kantoskop.c,55 :: 		Read_SHT11(&SHTtemperature, &SHTrel_humidity);
LUI	R26, hi_addr(_SHTrel_humidity+0)
ORI	R26, R26, lo_addr(_SHTrel_humidity+0)
LUI	R25, hi_addr(_SHTtemperature+0)
ORI	R25, R25, lo_addr(_SHTtemperature+0)
JAL	_Read_SHT11+0
NOP	
;Kantoskop.c,56 :: 		}
L_main6:
;Kantoskop.c,57 :: 		if (tmp == 'S') {
LBU	R3, Offset(_tmp+0)(GP)
ORI	R2, R0, 83
BEQ	R3, R2, L__main12
NOP	
J	L_main7
NOP	
L__main12:
;Kantoskop.c,59 :: 		LATA1_bit = ~LATA1_bit;
_LX	
EXT	R2, R2, BitPos(LATA1_bit+0), 1
XORI	R3, R2, 1
_LX	
INS	R2, R3, BitPos(LATA1_bit+0), 1
_SX	
;Kantoskop.c,61 :: 		}
L_main7:
;Kantoskop.c,62 :: 		}
J	L_main4
NOP	
L_main5:
;Kantoskop.c,64 :: 		sprintf(UART_str,"%3.1f %3.1f %1.8f %i %2.1f \r\n", SHTtemperature, SHTrel_humidity, voltage,(int) LATA1_bit, ReadTempeartureDS1820(&PORTA, 0));
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_ReadTempeartureDS1820+0
NOP	
_LX	
EXT	R3, R3, BitPos(LATA1_bit+0), 1
ADDIU	SP, SP, -28
SW	R2, 24(SP)
SH	R3, 20(SP)
LW	R2, Offset(_voltage+0)(GP)
SW	R2, 16(SP)
LW	R2, Offset(_SHTrel_humidity+0)(GP)
SW	R2, 12(SP)
LW	R2, Offset(_SHTtemperature+0)(GP)
SW	R2, 8(SP)
LUI	R2, hi_addr(?lstr_1_Kantoskop+0)
ORI	R2, R2, lo_addr(?lstr_1_Kantoskop+0)
SW	R2, 4(SP)
LUI	R2, hi_addr(_UART_str+0)
ORI	R2, R2, lo_addr(_UART_str+0)
SW	R2, 0(SP)
JAL	_sprintf+0
NOP	
ADDIU	SP, SP, 28
;Kantoskop.c,66 :: 		UART2_Write_Text(UART_str);
LUI	R25, hi_addr(_UART_str+0)
ORI	R25, R25, lo_addr(_UART_str+0)
JAL	_UART2_Write_Text+0
NOP	
;Kantoskop.c,68 :: 		}
J	L_main2
NOP	
;Kantoskop.c,69 :: 		}
L_end_main:
L__main_end_loop:
J	L__main_end_loop
NOP	
; end of _main
