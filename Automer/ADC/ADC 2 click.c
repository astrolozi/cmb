/*******************************************************************************
 * Project name:
     ADC 2 click
 * Copyright:
     (c) Mikroelektronika, 2015
 * Revision History:
     20150722:
       - initial release (DM)
       - mikroC PRO for PIC (AM)
 * Description:
     ADC 2 click carries MCP3551/3, which is a 22-bit ADC with automatic internal 
     offset and gain calibration.This high precision analog-to-digital converter 
     has total unadjusted error of less than 10 ppm, and low-output noise of 2.5 �V RMS.
     The example is showing values of ADC module (MPC3551) 22-bit register.

 * Test configuration:
     MCU:             PIC18F45K22
                      http://ww1.microchip.com/downloads/en/DeviceDoc/41412D.pdf
     Dev.Board:       EasyPIC7
                      http://www.mikroe.com/eng/products/view/757/easypic-v7-development-system/
     Oscillator:      HS-PLL 32.0000 MHz, 8.0000 MHz Crystal
     Ext. modules:    ADC 2 click

     SW:              mikroC PRO for PIC
                      http://www.mikroe.com/mikroc/pic/
 * NOTES:

     - Place ADC 2 click board at the mikroBUS socket 1 on the EasyPIC7 board.
     - Put LCD in its socket, turn on backlight using the SW4.6

*******************************************************************************/

// Lcd module connections
sbit LCD_RS at LATB4_bit;
sbit LCD_EN at LATB5_bit;
sbit LCD_D4 at LATB0_bit;
sbit LCD_D5 at LATB1_bit;
sbit LCD_D6 at LATB2_bit;
sbit LCD_D7 at LATB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End Lcd module connections

// ADC click module connections
sbit ChipSelect at LATE0_bit;
sbit ChipSelect_Direction at TRISE0_bit;

unsigned int value;
long adcValue;
char strADC_Value[12];

/*******************************************************************************
* Function MCP3551_Read
* ------------------------------------------------------------------------------
* Overview: Read over SPI ADC value in MCP3551 register
* Input: Nothing
* Output: ADC Value
*******************************************************************************/
long MCP3551_Read() {
  char ADC_Byte1, ADC_Byte2, ADC_Byte3, timer = 0;
  long ADC_Value = 0;

  ChipSelect = 0;                      // CS_pin; Clear(port bit); put low
  Delay_us(100);                       // PowerUp Time (min.10us)

  while(SDI_bit) {                     // Conversion time: 75ms

    ChipSelect = 1;                    // CS_pin; Set(port bit); put high
    Delay_us(1);
    ChipSelect = 0;                    // CS_pin; Clear(port bit); put low
    timer++;
    Delay_ms(10);                      // Polling SDI_bit on every 10ms

    if (timer >= 20) {                 // Failsafe timer
      return -4000000;                 // No response from MCP3551;
    }
  }

  ADC_Byte3 = SPI1_Read(0);
  ADC_Byte2 = SPI1_Read(0);
  ADC_Byte1 = SPI1_Read(0);

  // enter shutdown (typ.10us)
  ChipSelect = 1;                      // CS_pin; Set(port bit); put high
  Delay_us(100);
  
  // store 24 bits in ADC_Value
  ADC_Value = ADC_Byte3;
  ADC_Value = (ADC_Value << 8) | ADC_Byte2;
  ADC_Value = (ADC_Value << 8) | ADC_Byte1;

  // OVH (false) condition
  if (ADC_Byte3 == 0x60 && ADC_Byte1 > 0)
    ADC_Value = 3000000;               // Vin+ >= Vref ; Vin- = GND

  // OVL (false) condition
  else if (ADC_Byte3 == 0x9F && ADC_Byte1 > 0){
    ADC_Value = -3000000;              // Vin- >= Vref ; Vin+ = GND
  }

  else {
   if ((ADC_Byte3 & 0x20) >> 5)       // Bit 21 = 1 ; value is negative
   ADC_Value = ADC_Value - 4194304;  // for 22 bit resolution
  }

 return ADC_Value;

}

//******************************************************************************
// Initialize MCU and SPI
//******************************************************************************
void Init(){

  ADCON1 = 0x0F;                       // Turn off analog inputs
  ANSELA = 0;
  ANSELC = 0;
  SLRCON = 0;                          // Set output slew rate on all ports at standard rate
  ChipSelect_Direction = 0;

  SPI1_Init_Advanced(_SPI_MASTER_OSC_DIV4, _SPI_DATA_SAMPLE_END, _SPI_CLK_IDLE_HIGH, _SPI_HIGH_2_LOW);

  Delay_ms(100);                       // Small brake for SPI to initialize

  Lcd_Init();                          // Initialize LCD display
  Lcd_Cmd(_LCD_CURSOR_OFF);            // Cursor off
  Lcd_Cmd(_LCD_CLEAR);                 // Clear display
}

//******************************************************************************
// Main function
//******************************************************************************
void main(){

  Init();
  Lcd_Out(1, 1, "ADC 2 Click Ex.");     // Display message on the Lcd
  Lcd_Out(2, 1, "Value: ");            // Display message on the Lcd

  while(1) {

    adcValue = MCP3551_Read();         // Read ADC value from MCP3551
    LongToStr(adcValue,strADC_Value);
    Ltrim(strADC_Value);
    Lcd_Out(2, 8, "         ");

    if (adcValue == -4000000) {
      Lcd_Out(2, 8, "Error");          // Reading Error
    }
    else if (adcValue == 3000000) {
      Lcd_Out(2, 8, "OVH");            // Overflow High
    }
    else if (adcValue == -3000000) {
      Lcd_Out(2, 8, "OVL");            // Overflow Low
    }
    else {
      Lcd_Out(2, 8, strADC_Value);     // Display message on the Lcd
    }

    Delay_ms(1000);
  }
}
//******************************************************************************
// End of Main function
//******************************************************************************