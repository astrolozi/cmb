_DrawScr:
;SHT11_click.c,59 :: 		void DrawScr(){
ADDIU	SP, SP, -20
SW	RA, 0(SP)
;SHT11_click.c,60 :: 		TFT_Fill_Screen(CL_WHITE);
SW	R25, 4(SP)
SW	R26, 8(SP)
SW	R27, 12(SP)
SW	R28, 16(SP)
ORI	R25, R0, 65535
JAL	_TFT_Fill_Screen+0
NOP	
;SHT11_click.c,61 :: 		TFT_Set_Pen(CL_Black, 1);
ORI	R26, R0, 1
MOVZ	R25, R0, R0
JAL	_TFT_Set_Pen+0
NOP	
;SHT11_click.c,62 :: 		TFT_Line(20, 220, 300, 220);
ORI	R28, R0, 220
ORI	R27, R0, 300
ORI	R26, R0, 220
ORI	R25, R0, 20
JAL	_TFT_Line+0
NOP	
;SHT11_click.c,63 :: 		TFT_LIne(20,  46, 300,  46);
ORI	R28, R0, 46
ORI	R27, R0, 300
ORI	R26, R0, 46
ORI	R25, R0, 20
JAL	_TFT_Line+0
NOP	
;SHT11_click.c,64 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_RED, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 63488
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,65 :: 		TFT_Write_Text("SHT1X click  TEST", 75, 14);
ORI	R27, R0, 14
ORI	R26, R0, 75
LUI	R25, hi_addr(?lstr1_SHT11_click+0)
ORI	R25, R25, lo_addr(?lstr1_SHT11_click+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,66 :: 		TFT_Set_Font(&Verdana12x13_Regular, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_Verdana12x13_Regular+0)
ORI	R25, R25, lo_addr(_Verdana12x13_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,67 :: 		TFT_Write_Text("EasyPIC Fusion v7", 19, 223);
ORI	R27, R0, 223
ORI	R26, R0, 19
LUI	R25, hi_addr(?lstr2_SHT11_click+0)
ORI	R25, R25, lo_addr(?lstr2_SHT11_click+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,68 :: 		TFT_Set_Font(&Verdana12x13_Regular, CL_RED, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 63488
LUI	R25, hi_addr(_Verdana12x13_Regular+0)
ORI	R25, R25, lo_addr(_Verdana12x13_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,69 :: 		TFT_Write_Text("www.mikroe.com", 200, 223);
ORI	R27, R0, 223
ORI	R26, R0, 200
LUI	R25, hi_addr(?lstr3_SHT11_click+0)
ORI	R25, R25, lo_addr(?lstr3_SHT11_click+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,70 :: 		TFT_Set_Font(&TFT_defaultFont, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_TFT_defaultFont+0)
ORI	R25, R25, lo_addr(_TFT_defaultFont+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,71 :: 		}
L_end_DrawScr:
LW	R28, 16(SP)
LW	R27, 12(SP)
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 20
JR	RA
NOP	
; end of _DrawScr
_Display_results:
;SHT11_click.c,76 :: 		void Display_results(){
ADDIU	SP, SP, -16
SW	RA, 0(SP)
;SHT11_click.c,79 :: 		sprintf(Temp_str, "T: %3.1f degC", temperature);
SW	R25, 4(SP)
SW	R26, 8(SP)
SW	R27, 12(SP)
LW	R2, Offset(_temperature+0)(GP)
ADDIU	SP, SP, -12
SW	R2, 8(SP)
LUI	R2, hi_addr(?lstr_4_SHT11_click+0)
ORI	R2, R2, lo_addr(?lstr_4_SHT11_click+0)
SW	R2, 4(SP)
LUI	R2, hi_addr(_Temp_str+0)
ORI	R2, R2, lo_addr(_Temp_str+0)
SW	R2, 0(SP)
JAL	_sprintf+0
NOP	
ADDIU	SP, SP, 12
;SHT11_click.c,80 :: 		sprintf(Humi_str, "RH: %3.1f pct", rel_humidity);
LW	R2, Offset(_rel_humidity+0)(GP)
ADDIU	SP, SP, -12
SW	R2, 8(SP)
LUI	R2, hi_addr(?lstr_5_SHT11_click+0)
ORI	R2, R2, lo_addr(?lstr_5_SHT11_click+0)
SW	R2, 4(SP)
LUI	R2, hi_addr(_Humi_str+0)
ORI	R2, R2, lo_addr(_Humi_str+0)
SW	R2, 0(SP)
JAL	_sprintf+0
NOP	
ADDIU	SP, SP, 12
;SHT11_click.c,82 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_WHITE, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 65535
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,83 :: 		TFT_Write_Text(Old_Temp_str, 20, 85);
ORI	R27, R0, 85
ORI	R26, R0, 20
LUI	R25, hi_addr(_Old_Temp_str+0)
ORI	R25, R25, lo_addr(_Old_Temp_str+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,84 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,85 :: 		TFT_Write_Text(Temp_str, 20, 85);
ORI	R27, R0, 85
ORI	R26, R0, 20
LUI	R25, hi_addr(_Temp_str+0)
ORI	R25, R25, lo_addr(_Temp_str+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,87 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_WHITE, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 65535
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,88 :: 		TFT_Write_Text(Old_Humi_str, 20, 145);
ORI	R27, R0, 145
ORI	R26, R0, 20
LUI	R25, hi_addr(_Old_Humi_str+0)
ORI	R25, R25, lo_addr(_Old_Humi_str+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,89 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;SHT11_click.c,90 :: 		TFT_Write_Text(Humi_str, 20, 145);
ORI	R27, R0, 145
ORI	R26, R0, 20
LUI	R25, hi_addr(_Humi_str+0)
ORI	R25, R25, lo_addr(_Humi_str+0)
JAL	_TFT_Write_Text+0
NOP	
;SHT11_click.c,92 :: 		for (i = 0; i < 16; i++){
; i start address is: 20 (R5)
MOVZ	R5, R0, R0
; i end address is: 20 (R5)
L_Display_results0:
; i start address is: 20 (R5)
ANDI	R2, R5, 255
SLTIU	R2, R2, 16
BNE	R2, R0, L__Display_results9
NOP	
J	L_Display_results1
NOP	
L__Display_results9:
;SHT11_click.c,93 :: 		Old_Temp_str[i] = Temp_str[i];
ANDI	R3, R5, 255
LUI	R2, hi_addr(_Old_Temp_str+0)
ORI	R2, R2, lo_addr(_Old_Temp_str+0)
ADDU	R4, R2, R3
ANDI	R3, R5, 255
LUI	R2, hi_addr(_Temp_str+0)
ORI	R2, R2, lo_addr(_Temp_str+0)
ADDU	R2, R2, R3
LBU	R2, 0(R2)
SB	R2, 0(R4)
;SHT11_click.c,94 :: 		Old_Humi_str[i] = Humi_str[i];
ANDI	R3, R5, 255
LUI	R2, hi_addr(_Old_Humi_str+0)
ORI	R2, R2, lo_addr(_Old_Humi_str+0)
ADDU	R4, R2, R3
ANDI	R3, R5, 255
LUI	R2, hi_addr(_Humi_str+0)
ORI	R2, R2, lo_addr(_Humi_str+0)
ADDU	R2, R2, R3
LBU	R2, 0(R2)
SB	R2, 0(R4)
;SHT11_click.c,92 :: 		for (i = 0; i < 16; i++){
ADDIU	R2, R5, 1
ANDI	R5, R2, 255
;SHT11_click.c,95 :: 		}
; i end address is: 20 (R5)
J	L_Display_results0
NOP	
L_Display_results1:
;SHT11_click.c,96 :: 		}
L_end_Display_results:
LW	R27, 12(SP)
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 16
JR	RA
NOP	
; end of _Display_results
_main:
;SHT11_click.c,98 :: 		void main() {
;SHT11_click.c,99 :: 		TFT_Init_ILI9341_8bit(320, 240);
ORI	R26, R0, 240
ORI	R25, R0, 320
JAL	_TFT_Init_ILI9341_8bit+0
NOP	
;SHT11_click.c,101 :: 		DrawScr();
JAL	_DrawScr+0
NOP	
;SHT11_click.c,103 :: 		SCL_Direction = 0;                 // SCL is output
LUI	R2, BitMask(TRISA2_bit+0)
ORI	R2, R2, BitMask(TRISA2_bit+0)
_SX	
;SHT11_click.c,104 :: 		SDA_Out_Pin = 1;                   // SDA is high
LUI	R2, BitMask(LATA3_bit+0)
ORI	R2, R2, BitMask(LATA3_bit+0)
_SX	
;SHT11_click.c,107 :: 		while(1)                           // Endless loop
L_main3:
;SHT11_click.c,109 :: 		Read_SHT11(&temperature, &rel_humidity);
LUI	R26, hi_addr(_rel_humidity+0)
ORI	R26, R26, lo_addr(_rel_humidity+0)
LUI	R25, hi_addr(_temperature+0)
ORI	R25, R25, lo_addr(_temperature+0)
JAL	_Read_SHT11+0
NOP	
;SHT11_click.c,110 :: 		Display_results();
JAL	_Display_results+0
NOP	
;SHT11_click.c,111 :: 		Delay_ms(800);                   // delay 800ms
LUI	R24, 325
ORI	R24, R24, 34132
L_main5:
ADDIU	R24, R24, -1
BNE	R24, R0, L_main5
NOP	
NOP	
NOP	
;SHT11_click.c,112 :: 		}
J	L_main3
NOP	
;SHT11_click.c,113 :: 		}
L_end_main:
L__main_end_loop:
J	L__main_end_loop
NOP	
; end of _main
