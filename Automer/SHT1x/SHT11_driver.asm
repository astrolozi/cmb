_s_transstart:
;SHT11_driver.c,45 :: 		void s_transstart() {
ADDIU	SP, SP, -4
SW	RA, 0(SP)
;SHT11_driver.c,46 :: 		SDA_Direction = 1;                                                            // define SDA as input
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
;SHT11_driver.c,47 :: 		SCL_Pin = 1;                                                                  // SCL high
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,48 :: 		Delay_1us();                                                                  // 1us delay
JAL	_Delay_1us+0
NOP	
;SHT11_driver.c,49 :: 		SDA_Direction = 0;                                                            // define SDA as output
_LX	
INS	R2, R0, BitPos(SDA_Direction+0), 1
_SX	
;SHT11_driver.c,50 :: 		SDA_Out_Pin = 0;                                                              // SDA low
_LX	
INS	R2, R0, BitPos(SDA_Out_Pin+0), 1
_SX	
;SHT11_driver.c,51 :: 		Delay_1us();                                                                  // 1us delay
JAL	_Delay_1us+0
NOP	
;SHT11_driver.c,52 :: 		SCL_Pin = 0;                                                                  // SCL low
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,53 :: 		Delay_1us();                                                                  // 1us delay
JAL	_Delay_1us+0
NOP	
;SHT11_driver.c,54 :: 		SCL_Pin = 1;                                                                  // SCL high
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,55 :: 		Delay_1us();                                                                  // 1us delay
JAL	_Delay_1us+0
NOP	
;SHT11_driver.c,56 :: 		SDA_Direction = 1;                                                            // define SDA as input
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
;SHT11_driver.c,57 :: 		Delay_1us();                                                                  // 1us delay
JAL	_Delay_1us+0
NOP	
;SHT11_driver.c,58 :: 		SCL_Pin = 0;                                                                  // SCL low
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,59 :: 		}
L_end_s_transstart:
LW	RA, 0(SP)
ADDIU	SP, SP, 4
JR	RA
NOP	
; end of _s_transstart
_s_read_byte:
;SHT11_driver.c,64 :: 		unsigned char s_read_byte(unsigned char ack)
;SHT11_driver.c,66 :: 		unsigned char i=0x80;
; i start address is: 12 (R3)
ORI	R3, R0, 128
;SHT11_driver.c,67 :: 		unsigned char val=0;
; val start address is: 16 (R4)
MOVZ	R4, R0, R0
;SHT11_driver.c,70 :: 		SDA_Direction = 1;      // release DATA-line
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
;SHT11_driver.c,71 :: 		SDA_Out_Pin = 1;
_LX	
ORI	R2, R2, BitMask(SDA_Out_Pin+0)
_SX	
;SHT11_driver.c,72 :: 		SCL_pin = 0;            // SCL Low
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
; val end address is: 16 (R4)
; i end address is: 12 (R3)
;SHT11_driver.c,74 :: 		while(i)                // shift bit for masking
L_s_read_byte0:
; val start address is: 16 (R4)
; i start address is: 12 (R3)
BNE	R3, R0, L__s_read_byte59
NOP	
J	L_s_read_byte1
NOP	
L__s_read_byte59:
;SHT11_driver.c,76 :: 		SCL_pin = 1;          // clk for SENSI-BUS
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,77 :: 		Delay_uS(1);
LUI	R24, 0
ORI	R24, R24, 26
L_s_read_byte2:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_read_byte2
NOP	
;SHT11_driver.c,78 :: 		if (SDA_In_Pin == 1)
_LX	
EXT	R2, R2, BitPos(SDA_In_Pin+0), 1
BNE	R2, 1, L__s_read_byte61
NOP	
J	L__s_read_byte51
NOP	
L__s_read_byte61:
;SHT11_driver.c,80 :: 		val=(val | i);      // read bit
OR	R2, R4, R3
ANDI	R4, R2, 255
; val end address is: 16 (R4)
;SHT11_driver.c,81 :: 		}
J	L_s_read_byte4
NOP	
L__s_read_byte51:
;SHT11_driver.c,78 :: 		if (SDA_In_Pin == 1)
;SHT11_driver.c,81 :: 		}
L_s_read_byte4:
;SHT11_driver.c,82 :: 		SCL_pin = 0;
; val start address is: 16 (R4)
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,83 :: 		Delay_uS(1);
LUI	R24, 0
ORI	R24, R24, 26
L_s_read_byte5:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_read_byte5
NOP	
;SHT11_driver.c,84 :: 		i=(i>>1);
ANDI	R2, R3, 255
; i end address is: 12 (R3)
SRL	R2, R2, 1
; i start address is: 12 (R3)
ANDI	R3, R2, 255
;SHT11_driver.c,85 :: 		}
; i end address is: 12 (R3)
J	L_s_read_byte0
NOP	
L_s_read_byte1:
;SHT11_driver.c,87 :: 		SDA_Direction = 0;
_LX	
INS	R2, R0, BitPos(SDA_Direction+0), 1
_SX	
;SHT11_driver.c,89 :: 		if (ack)
BNE	R25, R0, L__s_read_byte63
NOP	
J	L_s_read_byte7
NOP	
L__s_read_byte63:
;SHT11_driver.c,92 :: 		SDA_Out_Pin = 0;
_LX	
INS	R2, R0, BitPos(SDA_Out_Pin+0), 1
_SX	
;SHT11_driver.c,93 :: 		}
J	L_s_read_byte8
NOP	
L_s_read_byte7:
;SHT11_driver.c,96 :: 		SDA_Out_Pin = 1;
_LX	
ORI	R2, R2, BitMask(SDA_Out_Pin+0)
_SX	
;SHT11_driver.c,97 :: 		}
L_s_read_byte8:
;SHT11_driver.c,99 :: 		SCL_pin = 1;                //clk #9 for ack
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,100 :: 		Delay_uS(3);
LUI	R24, 0
ORI	R24, R24, 79
L_s_read_byte9:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_read_byte9
NOP	
NOP	
;SHT11_driver.c,101 :: 		SCL_pin = 0;
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,102 :: 		Delay_uS(1);
LUI	R24, 0
ORI	R24, R24, 26
L_s_read_byte11:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_read_byte11
NOP	
;SHT11_driver.c,104 :: 		SDA_Direction = 1;                           //release DATA-line
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
;SHT11_driver.c,105 :: 		SDA_Out_Pin = 1;
_LX	
ORI	R2, R2, BitMask(SDA_Out_Pin+0)
_SX	
;SHT11_driver.c,107 :: 		return (val);
ANDI	R2, R4, 255
; val end address is: 16 (R4)
;SHT11_driver.c,108 :: 		}
L_end_s_read_byte:
JR	RA
NOP	
; end of _s_read_byte
_s_write_byte:
;SHT11_driver.c,113 :: 		unsigned char s_write_byte(unsigned char value)
;SHT11_driver.c,115 :: 		unsigned char i=0x80;
; i start address is: 16 (R4)
ORI	R4, R0, 128
;SHT11_driver.c,116 :: 		unsigned char error=0;
; error start address is: 12 (R3)
MOVZ	R3, R0, R0
;SHT11_driver.c,118 :: 		SDA_Direction = 0;
_LX	
INS	R2, R0, BitPos(SDA_Direction+0), 1
_SX	
; i end address is: 16 (R4)
; error end address is: 12 (R3)
;SHT11_driver.c,120 :: 		while(i)
L_s_write_byte13:
; error start address is: 12 (R3)
; i start address is: 16 (R4)
BNE	R4, R0, L__s_write_byte66
NOP	
J	L_s_write_byte14
NOP	
L__s_write_byte66:
;SHT11_driver.c,122 :: 		if (i & value)
AND	R2, R4, R25
BNE	R2, R0, L__s_write_byte68
NOP	
J	L_s_write_byte15
NOP	
L__s_write_byte68:
;SHT11_driver.c,124 :: 		SDA_Out_Pin = 1;    //masking value with i , write to SENSI-BUS
_LX	
ORI	R2, R2, BitMask(SDA_Out_Pin+0)
_SX	
;SHT11_driver.c,125 :: 		}
J	L_s_write_byte16
NOP	
L_s_write_byte15:
;SHT11_driver.c,128 :: 		SDA_Out_Pin = 0;
_LX	
INS	R2, R0, BitPos(SDA_Out_Pin+0), 1
_SX	
;SHT11_driver.c,129 :: 		}
L_s_write_byte16:
;SHT11_driver.c,131 :: 		SCL_pin = 1;  //clk for SENSI-BUS
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,132 :: 		Delay_uS(3);
LUI	R24, 0
ORI	R24, R24, 79
L_s_write_byte17:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_write_byte17
NOP	
NOP	
;SHT11_driver.c,133 :: 		SCL_pin = 0;
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,134 :: 		Delay_uS(3);
LUI	R24, 0
ORI	R24, R24, 79
L_s_write_byte19:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_write_byte19
NOP	
NOP	
;SHT11_driver.c,135 :: 		i=(i>>1);
ANDI	R2, R4, 255
; i end address is: 16 (R4)
SRL	R2, R2, 1
; i start address is: 16 (R4)
ANDI	R4, R2, 255
;SHT11_driver.c,136 :: 		}
; i end address is: 16 (R4)
J	L_s_write_byte13
NOP	
L_s_write_byte14:
;SHT11_driver.c,138 :: 		SDA_Direction = 1;                           //release DATA-line
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
;SHT11_driver.c,139 :: 		SDA_Out_Pin = 1;
_LX	
ORI	R2, R2, BitMask(SDA_Out_Pin+0)
_SX	
;SHT11_driver.c,141 :: 		SCL_pin = 1;                           //clk #9 for ack
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,142 :: 		Delay_uS(3);
LUI	R24, 0
ORI	R24, R24, 79
L_s_write_byte21:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_write_byte21
NOP	
NOP	
;SHT11_driver.c,143 :: 		if (SDA_In_Pin == 1)  error = 1; //check ack (DATA will be pulled down by SHT11)
_LX	
EXT	R2, R2, BitPos(SDA_In_Pin+0), 1
BNE	R2, 1, L__s_write_byte70
NOP	
J	L__s_write_byte52
NOP	
L__s_write_byte70:
ORI	R3, R0, 1
; error end address is: 12 (R3)
J	L_s_write_byte23
NOP	
L__s_write_byte52:
L_s_write_byte23:
;SHT11_driver.c,144 :: 		Delay_uS(1);
; error start address is: 12 (R3)
LUI	R24, 0
ORI	R24, R24, 26
L_s_write_byte24:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_write_byte24
NOP	
;SHT11_driver.c,145 :: 		SCL_pin = 0;
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,147 :: 		return(error); //error=1 in case of no acknowledge
ANDI	R2, R3, 255
; error end address is: 12 (R3)
;SHT11_driver.c,148 :: 		}
L_end_s_write_byte:
JR	RA
NOP	
; end of _s_write_byte
_s_measure:
;SHT11_driver.c,156 :: 		unsigned char s_measure(unsigned int *p_value, unsigned char mode)
ADDIU	SP, SP, -8
SW	RA, 0(SP)
;SHT11_driver.c,158 :: 		unsigned char i=0;
; i start address is: 20 (R5)
MOVZ	R5, R0, R0
;SHT11_driver.c,162 :: 		*p_value=0;
SH	R0, 0(R25)
;SHT11_driver.c,163 :: 		s_transstart(); //transmission start
JAL	_s_transstart+0
NOP	
;SHT11_driver.c,165 :: 		if(mode)
BNE	R26, R0, L__s_measure73
NOP	
J	L_s_measure26
NOP	
L__s_measure73:
;SHT11_driver.c,167 :: 		mode = MEASURE_HUMI;
ORI	R26, R0, 5
;SHT11_driver.c,168 :: 		}
J	L_s_measure27
NOP	
L_s_measure26:
;SHT11_driver.c,171 :: 		mode = MEASURE_TEMP;
ORI	R26, R0, 3
;SHT11_driver.c,172 :: 		}
L_s_measure27:
;SHT11_driver.c,174 :: 		if (s_write_byte(mode)) return(1);
SW	R25, 4(SP)
ANDI	R25, R26, 255
JAL	_s_write_byte+0
NOP	
LW	R25, 4(SP)
BNE	R2, R0, L__s_measure75
NOP	
J	L_s_measure28
NOP	
L__s_measure75:
; i end address is: 20 (R5)
ORI	R2, R0, 1
J	L_end_s_measure
NOP	
L_s_measure28:
;SHT11_driver.c,177 :: 		SDA_Direction = 1;
; i start address is: 20 (R5)
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
; i end address is: 20 (R5)
ANDI	R3, R5, 255
;SHT11_driver.c,179 :: 		while(i<240)
L_s_measure29:
; i start address is: 12 (R3)
ANDI	R2, R3, 255
SLTIU	R2, R2, 240
BNE	R2, R0, L__s_measure76
NOP	
J	L__s_measure53
NOP	
L__s_measure76:
;SHT11_driver.c,181 :: 		Delay_mS(1);
LUI	R24, 0
ORI	R24, R24, 26666
L_s_measure31:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_measure31
NOP	
;SHT11_driver.c,182 :: 		Delay_mS(1);
LUI	R24, 0
ORI	R24, R24, 26666
L_s_measure33:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_measure33
NOP	
;SHT11_driver.c,183 :: 		Delay_mS(1);
LUI	R24, 0
ORI	R24, R24, 26666
L_s_measure35:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_measure35
NOP	
;SHT11_driver.c,184 :: 		if (SDA_In_Pin == 0)
_LX	
EXT	R2, R2, BitPos(SDA_In_Pin+0), 1
BEQ	R2, R0, L__s_measure77
NOP	
J	L_s_measure37
NOP	
L__s_measure77:
; i end address is: 12 (R3)
;SHT11_driver.c,186 :: 		i=0;
; i start address is: 8 (R2)
MOVZ	R2, R0, R0
;SHT11_driver.c,187 :: 		break;
; i end address is: 8 (R2)
J	L_s_measure30
NOP	
;SHT11_driver.c,188 :: 		}
L_s_measure37:
;SHT11_driver.c,189 :: 		i++;
; i start address is: 12 (R3)
ADDIU	R2, R3, 1
ANDI	R3, R2, 255
;SHT11_driver.c,190 :: 		}
; i end address is: 12 (R3)
J	L_s_measure29
NOP	
L__s_measure53:
;SHT11_driver.c,179 :: 		while(i<240)
ANDI	R2, R3, 255
;SHT11_driver.c,190 :: 		}
L_s_measure30:
;SHT11_driver.c,193 :: 		if(i) return(2);
; i start address is: 8 (R2)
BNE	R2, R0, L__s_measure79
NOP	
J	L_s_measure38
NOP	
L__s_measure79:
; i end address is: 8 (R2)
ORI	R2, R0, 2
J	L_end_s_measure
NOP	
L_s_measure38:
;SHT11_driver.c,195 :: 		msb=s_read_byte(ACK); //read the first byte (MSB)
SW	R25, 4(SP)
ORI	R25, R0, 1
JAL	_s_read_byte+0
NOP	
; msb start address is: 24 (R6)
ANDI	R6, R2, 255
;SHT11_driver.c,196 :: 		lsb=s_read_byte(ACK); //read the second byte (LSB)
ORI	R25, R0, 1
JAL	_s_read_byte+0
NOP	
; lsb start address is: 20 (R5)
ANDI	R5, R2, 255
;SHT11_driver.c,197 :: 		checksum=s_read_byte(noACK);                //read checksum (8-bit)
MOVZ	R25, R0, R0
JAL	_s_read_byte+0
NOP	
LW	R25, 4(SP)
;SHT11_driver.c,199 :: 		*p_value=(msb<<8)|(lsb);
ANDI	R2, R6, 255
; msb end address is: 24 (R6)
SLL	R3, R2, 8
ANDI	R2, R5, 255
; lsb end address is: 20 (R5)
OR	R2, R3, R2
SH	R2, 0(R25)
;SHT11_driver.c,201 :: 		return(0);
MOVZ	R2, R0, R0
;SHT11_driver.c,202 :: 		}
L_end_s_measure:
LW	RA, 0(SP)
ADDIU	SP, SP, 8
JR	RA
NOP	
; end of _s_measure
_calc_sth11_temp:
;SHT11_driver.c,209 :: 		float calc_sth11_temp(unsigned int t)
ADDIU	SP, SP, -4
SW	RA, 0(SP)
;SHT11_driver.c,212 :: 		t_out =  t*0.01 - 40;
ANDI	R4, R25, 65535
JAL	__Unsigned16IntToFloat+0
NOP	
LUI	R4, 15395
ORI	R4, R4, 55050
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
LUI	R6, 16928
ORI	R6, R6, 0
MOVZ	R4, R2, R0
JAL	__Sub_FP+0
NOP	
;SHT11_driver.c,214 :: 		return t_out;
;SHT11_driver.c,215 :: 		}
L_end_calc_sth11_temp:
LW	RA, 0(SP)
ADDIU	SP, SP, 4
JR	RA
NOP	
; end of _calc_sth11_temp
_calc_sth11_humi:
;SHT11_driver.c,222 :: 		float calc_sth11_humi(unsigned int h, int t)
ADDIU	SP, SP, -12
SW	RA, 0(SP)
;SHT11_driver.c,228 :: 		t_C=t*0.01 - 40;                          //calc. temperature from ticks to [�C]
SEH	R4, R26
JAL	__SignedIntegralToFLoat+0
NOP	
LUI	R4, 15395
ORI	R4, R4, 55050
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
LUI	R6, 16928
ORI	R6, R6, 0
MOVZ	R4, R2, R0
JAL	__Sub_FP+0
NOP	
; t_C start address is: 64 (R16)
MOVZ	R16, R2, R0
;SHT11_driver.c,229 :: 		rh_lin=C3*h*h + C2*h + C1;                //calc. humidity from ticks to [%RH]
ANDI	R4, R25, 65535
JAL	__Unsigned16IntToFloat+0
NOP	
SW	R2, 8(SP)
LUI	R4, 46651
ORI	R4, R4, 59298
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
LW	R4, 8(SP)
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
SW	R2, 4(SP)
LUI	R6, 15653
ORI	R6, R6, 58196
LW	R4, 8(SP)
JAL	__Mul_FP+0
NOP	
LW	R4, 4(SP)
MOVZ	R6, R2, R0
JAL	__Add_FP+0
NOP	
LUI	R4, 49280
ORI	R4, R4, 0
MOVZ	R6, R2, R0
JAL	__Add_FP+0
NOP	
; rh_lin start address is: 12 (R3)
MOVZ	R3, R2, R0
;SHT11_driver.c,230 :: 		rh_true=(t_C-25)*(T1+T2*h)+rh_lin;        //calc. temperature compensated humidity
LUI	R6, 16840
ORI	R6, R6, 0
MOVZ	R4, R16, R0
JAL	__Sub_FP+0
NOP	
; t_C end address is: 64 (R16)
SW	R2, 4(SP)
LUI	R6, 14503
ORI	R6, R6, 50604
LW	R4, 8(SP)
JAL	__Mul_FP+0
NOP	
LUI	R4, 15395
ORI	R4, R4, 55050
MOVZ	R6, R2, R0
JAL	__Add_FP+0
NOP	
LW	R4, 4(SP)
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
MOVZ	R4, R2, R0
MOVZ	R6, R3, R0
JAL	__Add_FP+0
NOP	
; rh_lin end address is: 12 (R3)
; rh_true start address is: 12 (R3)
MOVZ	R3, R2, R0
;SHT11_driver.c,239 :: 		if(rh_true>100)rh_true=100;               //cut if the value is outside of
LUI	R4, 17096
ORI	R4, R4, 0
MOVZ	R6, R2, R0
JAL	__Compare_FP+0
NOP	
SLTI	R2, R2, 0
BNE	R2, R0, L__calc_sth11_humi84
NOP	
J	L__calc_sth11_humi54
NOP	
L__calc_sth11_humi84:
LUI	R3, 17096
ORI	R3, R3, 0
; rh_true end address is: 12 (R3)
J	L_calc_sth11_humi39
NOP	
L__calc_sth11_humi54:
L_calc_sth11_humi39:
;SHT11_driver.c,240 :: 		if(rh_true<0.1)rh_true=0.1;               //the physical possible range
; rh_true start address is: 12 (R3)
LUI	R4, 15820
ORI	R4, R4, 52429
MOVZ	R6, R3, R0
JAL	__Compare_FP+0
NOP	
SLT	R2, R0, R2
BNE	R2, R0, L__calc_sth11_humi87
NOP	
J	L__calc_sth11_humi55
NOP	
L__calc_sth11_humi87:
LUI	R3, 15820
ORI	R3, R3, 52429
; rh_true end address is: 12 (R3)
J	L_calc_sth11_humi40
NOP	
L__calc_sth11_humi55:
L_calc_sth11_humi40:
;SHT11_driver.c,242 :: 		return rh_true;
; rh_true start address is: 12 (R3)
MOVZ	R2, R3, R0
; rh_true end address is: 12 (R3)
;SHT11_driver.c,243 :: 		}
L_end_calc_sth11_humi:
LW	RA, 0(SP)
ADDIU	SP, SP, 12
JR	RA
NOP	
; end of _calc_sth11_humi
_Read_SHT11:
;SHT11_driver.c,248 :: 		void Read_SHT11(float *fT, float *fRH)
ADDIU	SP, SP, -16
SW	RA, 0(SP)
;SHT11_driver.c,254 :: 		ucSens_Error = 0;
SB	R0, Offset(_ucSens_Error+0)(GP)
;SHT11_driver.c,256 :: 		ucSens_Error = s_measure(&t, 0);
ADDIU	R2, SP, 12
SW	R26, 4(SP)
SW	R25, 8(SP)
MOVZ	R26, R0, R0
MOVZ	R25, R2, R0
JAL	_s_measure+0
NOP	
LW	R25, 8(SP)
LW	R26, 4(SP)
SB	R2, Offset(_ucSens_Error+0)(GP)
;SHT11_driver.c,257 :: 		iSHT_Temp = (int)(calc_sth11_temp(t) * 10);
SW	R25, 4(SP)
LHU	R25, 12(SP)
JAL	_calc_sth11_temp+0
NOP	
LW	R25, 4(SP)
LUI	R4, 16672
ORI	R4, R4, 0
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
MOVZ	R4, R2, R0
JAL	__FloatToSignedIntegral+0
NOP	
SH	R2, Offset(_iSHT_Temp+0)(GP)
;SHT11_driver.c,259 :: 		ucSens_Error = s_measure(&h, 1);
ADDIU	R2, SP, 14
SW	R26, 4(SP)
SW	R25, 8(SP)
ORI	R26, R0, 1
MOVZ	R25, R2, R0
JAL	_s_measure+0
NOP	
SB	R2, Offset(_ucSens_Error+0)(GP)
;SHT11_driver.c,260 :: 		iSHT_Humi = (int)(calc_sth11_humi(h, t) * 10);
LHU	R26, 12(SP)
LHU	R25, 14(SP)
JAL	_calc_sth11_humi+0
NOP	
LW	R25, 8(SP)
LW	R26, 4(SP)
LUI	R4, 16672
ORI	R4, R4, 0
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
MOVZ	R4, R2, R0
JAL	__FloatToSignedIntegral+0
NOP	
SH	R2, Offset(_iSHT_Humi+0)(GP)
;SHT11_driver.c,262 :: 		value = (float)iSHT_Temp;
LH	R4, Offset(_iSHT_Temp+0)(GP)
JAL	__SignedIntegralToFLoat+0
NOP	
;SHT11_driver.c,263 :: 		*fT = value / 10;
LUI	R6, 16672
ORI	R6, R6, 0
MOVZ	R4, R2, R0
JAL	__Div_FP+0
NOP	
SW	R2, 0(R25)
;SHT11_driver.c,264 :: 		value = (float)iSHT_Humi;
LH	R4, Offset(_iSHT_Humi+0)(GP)
JAL	__SignedIntegralToFLoat+0
NOP	
;SHT11_driver.c,265 :: 		*fRH = value / 10;
LUI	R6, 16672
ORI	R6, R6, 0
MOVZ	R4, R2, R0
JAL	__Div_FP+0
NOP	
SW	R2, 0(R26)
;SHT11_driver.c,266 :: 		}
L_end_Read_SHT11:
LW	RA, 0(SP)
ADDIU	SP, SP, 16
JR	RA
NOP	
; end of _Read_SHT11
_s_read_statusreg:
;SHT11_driver.c,271 :: 		char s_read_statusreg(unsigned char *p_value)
ADDIU	SP, SP, -12
SW	RA, 0(SP)
;SHT11_driver.c,273 :: 		unsigned char checksum = 0;
SW	R25, 4(SP)
;SHT11_driver.c,275 :: 		s_transstart();                             //transmission start
JAL	_s_transstart+0
NOP	
;SHT11_driver.c,276 :: 		if(s_write_byte(STATUS_REG_R)) return 1;    //send command to sensor
SW	R25, 8(SP)
ORI	R25, R0, 7
JAL	_s_write_byte+0
NOP	
LW	R25, 8(SP)
BNE	R2, R0, L__s_read_statusreg91
NOP	
J	L_s_read_statusreg41
NOP	
L__s_read_statusreg91:
ORI	R2, R0, 1
J	L_end_s_read_statusreg
NOP	
L_s_read_statusreg41:
;SHT11_driver.c,277 :: 		*p_value=s_read_byte(ACK);                  //read status register (8-bit)
SW	R25, 8(SP)
ORI	R25, R0, 1
JAL	_s_read_byte+0
NOP	
LW	R25, 8(SP)
SB	R2, 0(R25)
;SHT11_driver.c,278 :: 		checksum=s_read_byte(noACK);                //read checksum (8-bit)
MOVZ	R25, R0, R0
JAL	_s_read_byte+0
NOP	
;SHT11_driver.c,280 :: 		return 0;
MOVZ	R2, R0, R0
;SHT11_driver.c,281 :: 		}
;SHT11_driver.c,280 :: 		return 0;
;SHT11_driver.c,281 :: 		}
L_end_s_read_statusreg:
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 12
JR	RA
NOP	
; end of _s_read_statusreg
_s_write_statusreg:
;SHT11_driver.c,288 :: 		char s_write_statusreg(unsigned char value)
ADDIU	SP, SP, -8
SW	RA, 0(SP)
;SHT11_driver.c,290 :: 		s_transstart();                             //transmission start
JAL	_s_transstart+0
NOP	
;SHT11_driver.c,291 :: 		if(s_write_byte(STATUS_REG_W)) return 1;    //send command to sensor
SB	R25, 4(SP)
ORI	R25, R0, 6
JAL	_s_write_byte+0
NOP	
LBU	R25, 4(SP)
BNE	R2, R0, L__s_write_statusreg94
NOP	
J	L_s_write_statusreg42
NOP	
L__s_write_statusreg94:
ORI	R2, R0, 1
J	L_end_s_write_statusreg
NOP	
L_s_write_statusreg42:
;SHT11_driver.c,292 :: 		if(s_write_byte(value)) return 1;           //send value of status register
JAL	_s_write_byte+0
NOP	
BNE	R2, R0, L__s_write_statusreg96
NOP	
J	L_s_write_statusreg43
NOP	
L__s_write_statusreg96:
ORI	R2, R0, 1
J	L_end_s_write_statusreg
NOP	
L_s_write_statusreg43:
;SHT11_driver.c,294 :: 		return 0;
MOVZ	R2, R0, R0
;SHT11_driver.c,295 :: 		}
L_end_s_write_statusreg:
LW	RA, 0(SP)
ADDIU	SP, SP, 8
JR	RA
NOP	
; end of _s_write_statusreg
_s_connectionreset:
;SHT11_driver.c,304 :: 		void s_connectionreset()
ADDIU	SP, SP, -4
SW	RA, 0(SP)
;SHT11_driver.c,309 :: 		SDA_Direction = 1;                            //release DATA-line
_LX	
ORI	R2, R2, BitMask(SDA_Direction+0)
_SX	
;SHT11_driver.c,310 :: 		SDA_Out_Pin = 1;
_LX	
ORI	R2, R2, BitMask(SDA_Out_Pin+0)
_SX	
;SHT11_driver.c,311 :: 		SCL_pin = 0;                           // SCL Low
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,313 :: 		for(i=0; i<9; i++)                     //9 SCK cycles
; i start address is: 12 (R3)
MOVZ	R3, R0, R0
; i end address is: 12 (R3)
L_s_connectionreset44:
; i start address is: 12 (R3)
ANDI	R2, R3, 255
SLTIU	R2, R2, 9
BNE	R2, R0, L__s_connectionreset98
NOP	
J	L_s_connectionreset45
NOP	
L__s_connectionreset98:
;SHT11_driver.c,315 :: 		SCL_pin = 1;
_LX	
ORI	R2, R2, BitMask(SCL_Pin+0)
_SX	
;SHT11_driver.c,316 :: 		Delay_uS(3);
LUI	R24, 0
ORI	R24, R24, 79
L_s_connectionreset47:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_connectionreset47
NOP	
NOP	
;SHT11_driver.c,317 :: 		SCL_pin = 0;
_LX	
INS	R2, R0, BitPos(SCL_Pin+0), 1
_SX	
;SHT11_driver.c,318 :: 		Delay_uS(3);
LUI	R24, 0
ORI	R24, R24, 79
L_s_connectionreset49:
ADDIU	R24, R24, -1
BNE	R24, R0, L_s_connectionreset49
NOP	
NOP	
;SHT11_driver.c,313 :: 		for(i=0; i<9; i++)                     //9 SCK cycles
ADDIU	R2, R3, 1
ANDI	R3, R2, 255
;SHT11_driver.c,319 :: 		}
; i end address is: 12 (R3)
J	L_s_connectionreset44
NOP	
L_s_connectionreset45:
;SHT11_driver.c,321 :: 		s_transstart();                        //transmission start
JAL	_s_transstart+0
NOP	
;SHT11_driver.c,322 :: 		}
L_end_s_connectionreset:
LW	RA, 0(SP)
ADDIU	SP, SP, 4
JR	RA
NOP	
; end of _s_connectionreset
_s_softreset:
;SHT11_driver.c,327 :: 		unsigned char s_softreset(void)
ADDIU	SP, SP, -8
SW	RA, 0(SP)
;SHT11_driver.c,329 :: 		s_connectionreset();                         //reset communication
SW	R25, 4(SP)
JAL	_s_connectionreset+0
NOP	
;SHT11_driver.c,331 :: 		return (s_write_byte(RESET));                //return=1 in case of no response form the sensor
ORI	R25, R0, 30
JAL	_s_write_byte+0
NOP	
;SHT11_driver.c,332 :: 		}
;SHT11_driver.c,331 :: 		return (s_write_byte(RESET));                //return=1 in case of no response form the sensor
;SHT11_driver.c,332 :: 		}
L_end_s_softreset:
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 8
JR	RA
NOP	
; end of _s_softreset
