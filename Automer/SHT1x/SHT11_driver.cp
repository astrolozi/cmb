#line 1 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"

extern sfr sbit SDA_Out_Pin;
extern sfr sbit SDA_In_Pin;
extern sfr sbit SCL_Pin;

extern sfr sbit SDA_Direction;
extern sfr sbit SCL_Direction;




const float C1=-4.0;
const float C2=+0.0405;
const float C3=-0.0000028;
const float T1=+0.01;
const float T2=+0.00008;
#line 29 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
unsigned char ucSens_Error;

int iSHT_Temp;
int iSHT_Humi;
#line 45 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
void s_transstart() {
 SDA_Direction = 1;
 SCL_Pin = 1;
 Delay_1us();
 SDA_Direction = 0;
 SDA_Out_Pin = 0;
 Delay_1us();
 SCL_Pin = 0;
 Delay_1us();
 SCL_Pin = 1;
 Delay_1us();
 SDA_Direction = 1;
 Delay_1us();
 SCL_Pin = 0;
}
#line 64 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
unsigned char s_read_byte(unsigned char ack)
{
 unsigned char i=0x80;
 unsigned char val=0;


 SDA_Direction = 1;
 SDA_Out_Pin = 1;
 SCL_pin = 0;

 while(i)
 {
 SCL_pin = 1;
 Delay_uS(1);
 if (SDA_In_Pin == 1)
 {
 val=(val | i);
 }
 SCL_pin = 0;
 Delay_uS(1);
 i=(i>>1);
 }

 SDA_Direction = 0;

 if (ack)
 {

 SDA_Out_Pin = 0;
 }
 else
 {
 SDA_Out_Pin = 1;
 }

 SCL_pin = 1;
 Delay_uS(3);
 SCL_pin = 0;
 Delay_uS(1);

 SDA_Direction = 1;
 SDA_Out_Pin = 1;

 return (val);
}
#line 113 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
unsigned char s_write_byte(unsigned char value)
{
 unsigned char i=0x80;
 unsigned char error=0;

 SDA_Direction = 0;

 while(i)
 {
 if (i & value)
 {
 SDA_Out_Pin = 1;
 }
 else
 {
 SDA_Out_Pin = 0;
 }

 SCL_pin = 1;
 Delay_uS(3);
 SCL_pin = 0;
 Delay_uS(3);
 i=(i>>1);
 }

 SDA_Direction = 1;
 SDA_Out_Pin = 1;

 SCL_pin = 1;
 Delay_uS(3);
 if (SDA_In_Pin == 1) error = 1;
 Delay_uS(1);
 SCL_pin = 0;

 return(error);
}
#line 156 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
unsigned char s_measure(unsigned int *p_value, unsigned char mode)
{
 unsigned char i=0;
 unsigned char msb,lsb;
 unsigned char checksum;

 *p_value=0;
 s_transstart();

 if(mode)
 {
 mode =  0x05 ;
 }
 else
 {
 mode =  0x03 ;
 }

 if (s_write_byte(mode)) return(1);


 SDA_Direction = 1;

 while(i<240)
 {
 Delay_mS(1);
 Delay_mS(1);
 Delay_mS(1);
 if (SDA_In_Pin == 0)
 {
 i=0;
 break;
 }
 i++;
 }


 if(i) return(2);

 msb=s_read_byte( 1 );
 lsb=s_read_byte( 1 );
 checksum=s_read_byte( 0 );

 *p_value=(msb<<8)|(lsb);

 return(0);
}
#line 209 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
float calc_sth11_temp(unsigned int t)
{
 float t_out;
 t_out = t*0.01 - 40;

 return t_out;
}
#line 222 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
float calc_sth11_humi(unsigned int h, int t)
{
 float rh_lin;
 float rh_true;
 float t_C;

 t_C=t*0.01 - 40;
 rh_lin=C3*h*h + C2*h + C1;
 rh_true=(t_C-25)*(T1+T2*h)+rh_lin;








 if(rh_true>100)rh_true=100;
 if(rh_true<0.1)rh_true=0.1;

 return rh_true;
}
#line 248 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
void Read_SHT11(float *fT, float *fRH)
{
 unsigned int t;
 unsigned int h;
 float value;

 ucSens_Error = 0;

 ucSens_Error = s_measure(&t, 0);
 iSHT_Temp = (int)(calc_sth11_temp(t) * 10);

 ucSens_Error = s_measure(&h, 1);
 iSHT_Humi = (int)(calc_sth11_humi(h, t) * 10);

 value = (float)iSHT_Temp;
 *fT = value / 10;
 value = (float)iSHT_Humi;
 *fRH = value / 10;
}
#line 271 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
char s_read_statusreg(unsigned char *p_value)
{
 unsigned char checksum = 0;

 s_transstart();
 if(s_write_byte( 0x07 )) return 1;
 *p_value=s_read_byte( 1 );
 checksum=s_read_byte( 0 );

 return 0;
}
#line 288 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
char s_write_statusreg(unsigned char value)
{
 s_transstart();
 if(s_write_byte( 0x06 )) return 1;
 if(s_write_byte(value)) return 1;

 return 0;
}
#line 304 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
void s_connectionreset()
{
 unsigned char i;


 SDA_Direction = 1;
 SDA_Out_Pin = 1;
 SCL_pin = 0;

 for(i=0; i<9; i++)
 {
 SCL_pin = 1;
 Delay_uS(3);
 SCL_pin = 0;
 Delay_uS(3);
 }

 s_transstart();
}
#line 327 "F:/Documents/cmb/Automer/SHT1x/SHT11_driver.c"
unsigned char s_softreset(void)
{
 s_connectionreset();

 return (s_write_byte( 0x1e ));
}
