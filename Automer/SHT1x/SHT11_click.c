/*
 * Project name:
     SHT11_click (Demonstration of mikroBUS SHT11 click board)
 * Copyright:
     (c) Mikroelektronika, 2012.
 * Revision History:
     20120521:
       - initial release (DO and JK);
 * Description:
     This code demonstrates how to use mikroBUS board SHT11 click
     SHT1x semsor uses I2C communication and measures temperature and relative humidity.
 * Test configuration:
     MCU:             P32MX795F512L
                      http://ww1.microchip.com/downloads/en/DeviceDoc/61156F.pdf
     Dev.Board:       EasyPIC Fusion v7
                      http://www.mikroe.com/easypic-fusion/
     Oscillator:      XT-PLL, 80.000MHz
     Ext. Modules:    SHT11 click :  ac:SHT1x_click
                      http://www.mikroe.com/click/sht1x
                      EasyTFT - ac:EasyTFT
                      http://www.mikroe.com/add-on-boards/display/easytft/
     SW:              mikroC PRO for PIC32
                      http://www.mikroe.com/mikroc/pic32/
 * NOTES:
     - Place SHT11 click board in mikroBUS socket 1.
     - Turn on I2C lines (RA2 and RA3) on SW14.
     - Pull-up RA2 and RA3 pins.
     - Turn on TFT backlight switch SW11.1.
 */

#include "resources.h"

// SHT11 connections
sbit SDA_In_Pin  at RA3_bit;        // Serial data input pin
sbit SDA_Out_Pin at LATA3_bit;      // Serial data output pin
sbit SCL_Pin     at LATA2_bit;      // Serial clock pin

sbit SDA_Direction at TRISA3_bit;   // Serial data direction pin
sbit SCL_Direction at TRISA2_bit;   // Serial clock direction pin
// End SHT11 connections

// Lcd constants
char txt1[] = "mikroElektronika";
char txt2[] = "SHT11 click";

// global variables
float temperature;
float rel_humidity;

char Temp_str[16], Old_Temp_str[16];
char Humi_str[16], Old_Humi_str[16];

void Read_SHT11(float *fT, float *fRH);
int oldvalue[2] = {0, 0};

/**************************************************************************************************
* Draw Screen
**************************************************************************************************/
void DrawScr(){
  TFT_Fill_Screen(CL_WHITE);
  TFT_Set_Pen(CL_Black, 1);
  TFT_Line(20, 220, 300, 220);
  TFT_LIne(20,  46, 300,  46);
  TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_RED, FO_HORIZONTAL);
  TFT_Write_Text("SHT1X click  TEST", 75, 14);
  TFT_Set_Font(&Verdana12x13_Regular, CL_BLACK, FO_HORIZONTAL);
  TFT_Write_Text("EasyPIC Fusion v7", 19, 223);
  TFT_Set_Font(&Verdana12x13_Regular, CL_RED, FO_HORIZONTAL);
  TFT_Write_Text("www.mikroe.com", 200, 223);
  TFT_Set_Font(&TFT_defaultFont, CL_BLACK, FO_HORIZONTAL);
}

/**************************************************************************************************
* displays result on TFT
**************************************************************************************************/
void Display_results(){
  char i;
  
  sprintf(Temp_str, "T: %3.1f degC", temperature);
  sprintf(Humi_str, "RH: %3.1f pct", rel_humidity);
  
  TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_WHITE, FO_HORIZONTAL);
  TFT_Write_Text(Old_Temp_str, 20, 85);
  TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_BLACK, FO_HORIZONTAL);
  TFT_Write_Text(Temp_str, 20, 85);
  
  TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_WHITE, FO_HORIZONTAL);
  TFT_Write_Text(Old_Humi_str, 20, 145);
  TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_BLACK, FO_HORIZONTAL);
  TFT_Write_Text(Humi_str, 20, 145);
  
  for (i = 0; i < 16; i++){
    Old_Temp_str[i] = Temp_str[i];
    Old_Humi_str[i] = Humi_str[i];
  }
}

void main() {
  TFT_Init_ILI9341_8bit(320, 240);

  DrawScr();

  SCL_Direction = 0;                 // SCL is output
  SDA_Out_Pin = 1;                   // SDA is high

  // Start the test example
  while(1)                           // Endless loop
  {
    Read_SHT11(&temperature, &rel_humidity);
    Display_results();
    Delay_ms(800);                   // delay 800ms
  }
}