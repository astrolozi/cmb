// SHT11 connections
sbit SDA_In_Pin  at RA3_bit;        // Serial data input pin
sbit SDA_Out_Pin at LATA3_bit;      // Serial data output pin
sbit SCL_Pin     at LATA2_bit;      // Serial clock pin

sbit SDA_Direction at TRISA3_bit;   // Serial data direction pin
sbit SCL_Direction at TRISA2_bit;   // Serial clock direction pin


// global variables
float SHTtemperature;
float SHTrel_humidity;

void Read_SHT11(float *fT, float *fRH);

char tmp;

//End of SHT setup
//ADC setup
// ADC click module connections
sbit ChipSelect at LATC4_bit;
sbit ChipSelect_Direction at TRISC4_bit;

void MCP3551_Init();
long MCP3551_Read();
double voltage;

//DS1820 setup
float ReadTempeartureDS1820(char* port, char pin);

char UART_str[30];

void main() {
    CHECON = 30;
    AD1PCFG = 0xFFFF;
    AD1PCFG = 0xFFFF;            // Configure AN pins as digital I/O
    JTAGEN_bit = 0;              // Disable JTAG
    UART2_Init(256000);              // Initialize UART module at 9600 bps
    Delay_ms(100);                  // Wait for UART module to stabilize
    //SHT11 click initialization
    SCL_Direction = 0;                 // SCL is output
    SDA_Out_Pin = 1;                   // SDA is high
    //ADC
    MCP3551_Init();
    Read_SHT11(&SHTtemperature, &SHTrel_humidity);
    
    TRISA1_bit = 0;
    LATA1_bit = 0;
    while(1){
       voltage = (MCP3551_Read() / 4194304.0) * 6.6;

       while(UART2_Data_Ready()){
         tmp = UART2_Read();
         if(tmp == 'R'){
           Read_SHT11(&SHTtemperature, &SHTrel_humidity);
         }
         if (tmp == 'S') {

           LATA1_bit = ~LATA1_bit;

         }
       }

       sprintf(UART_str,"%3.1f %3.1f %1.8f %i %2.1f \r\n", SHTtemperature, SHTrel_humidity, voltage,(int) LATA1_bit, ReadTempeartureDS1820(&PORTA, 0));

       UART2_Write_Text(UART_str);

    }
}