extern sfr sbit ChipSelect;
extern sfr sbit ChipSelect_Direction;
void MCP3551_Init(){


  ChipSelect_Direction = 0;

  //SPI2_Init_Advanced(_SPI_MASTER_OSC_DIV4, _SPI_DATA_SAMPLE_END, _SPI_CLK_IDLE_HIGH, _SPI_HIGH_2_LOW);
  // Set SPI2 to the Master Mode, data length is 16-bit, clock = Fcy (no clock scaling), data sampled in the middle of interval, clock IDLE state high and data transmitted at low to high clock edge:
  SPI2_Init_Advanced(_SPI_MASTER, _SPI_8_BIT, 16, _SPI_SS_DISABLE, _SPI_DATA_SAMPLE_END, _SPI_CLK_IDLE_HIGH, _SPI_ACTIVE_2_IDLE);
  Delay_ms(100);                       // Small brake for SPI to initialize
}


long MCP3551_Read() {
  char ADC_Byte1, ADC_Byte2, ADC_Byte3, timer = 0;
  long ADC_Value = 0;

  ChipSelect = 0;                      // CS_pin; Clear(port bit); put low
  Delay_us(100);                       // PowerUp Time (min.10us)

  while(RG7_bit) {                     // Conversion time: 75ms

    ChipSelect = 1;                    // CS_pin; Set(port bit); put high
    Delay_us(1);
    ChipSelect = 0;                    // CS_pin; Clear(port bit); put low
    timer++;
    Delay_ms(10);                      // Polling SDI_bit on every 10ms

    if (timer >= 20) {                 // Failsafe timer
      return -400000;                 // No response from MCP3551;
    }
  }
  ADC_Byte3 = SPI2_Read(0);
  ADC_Byte2 = SPI2_Read(0);
  ADC_Byte1 = SPI2_Read(0);

  // enter shutdown (typ.10us)
  ChipSelect = 1;                      // CS_pin; Set(port bit); put high
  Delay_us(100);

  // store 24 bits in ADC_Value
  ADC_Value = ADC_Byte3;
  ADC_Value = (ADC_Value << 8) | ADC_Byte2;
  ADC_Value = (ADC_Value << 8) | ADC_Byte1;

  // OVH (false) condition
  if (ADC_Byte3 == 0x60 && ADC_Byte1 > 0)
    ADC_Value = 3000000;               // Vin+ >= Vref ; Vin- = GND

  // OVL (false) condition
  else if (ADC_Byte3 == 0x9F && ADC_Byte1 > 0){
    ADC_Value = -3000000;              // Vin- >= Vref ; Vin+ = GND
  }

  else {
   if ((ADC_Byte3 & 0x20) >> 5)       // Bit 21 = 1 ; value is negative
   ADC_Value = ADC_Value - 4194304;  // for 22 bit resolution
  }

 return ADC_Value;

}