/*
 * Project name:
     OneWire (Interfacing the DS1820 temperature sensor - all versions)
 * Copyright:
     (c) Mikroelektronika, 2012.
 * Revision History:
     20120810:
       - initial release (FJ);
 * Description:
     This code demonstrates one-wire communication with temperature sensor
     DS18x20 connected to PB10 pin.
     MCU reads temperature from the sensor and prints it on the TFT.
     The display format of the temperature is 'xxx.xxxx�C'. To obtain correct
     results, the 18x20's temperature resolution has to be adjusted (constant
     TEMP_RESOLUTION).
* Test configuration:
     MCU:             P32MX460F512L
                      http://ww1.microchip.com/downloads/en/devicedoc/61143f.pdf
     Dev.Board:       EasyPIC Fusion v7 - ac:DS1820
                      http://www.mikroe.com/easypic-fusion/
     Oscillator:      XT-PLL, 80.000MHz
     Ext. Modules:    EasyTFT display - ac:EasyTFT
     SW:              mikroC PRO for PIC32
                      http://www.mikroe.com/eng/products/view/623/mikroc-pro-for-pic32/ 
 * NOTES:
     - Turn on LM35 switch at SW11.4. (board specific)
     - Turn on TFT backlight switch SW11.1. (board specific)
     - Turn off PORTB LEDs at SW15. (board specific)
 */

#include "Resources.h"

//  Set TEMP_RESOLUTION to the corresponding resolution of used DS18x20 sensor:
//  18S20: 9  (default setting; can be 9,10,11,or 12)
//  18B20: 12
const unsigned short TEMP_RESOLUTION = 9;

char *text = "000.0000  C";
char *text_old = "000.0000  C";
unsigned temp;

// MCU initialization
void Init() {
  AD1PCFG = 0xFFFF;            // Configure AN pins as digital I/O
  JTAGEN_bit = 0;              // Disable JTAG
  TFT_BLED_Direction = 0;      // Set TFT backlight pin as output
  TFT_Set_Default_Mode();
  TFT_Init_ILI9341_8bit(320, 240);    // Initialize TFT display
  TFT_BLED = 1;                // Turn on TFT backlight
}

void DrawFrame(){
  TFT_Fill_Screen(CL_WHITE);
  TFT_Set_Pen(CL_BLACK, 1);
  TFT_Line(20, 220, 300, 220);
  TFT_LIne(20,  46, 300,  46);
  TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_RED, FO_HORIZONTAL);
  TFT_Write_Text("Temperature  Sensor  (DS1820)", 15, 14);
  TFT_Set_Font(&Verdana12x13_Regular, CL_BLACK, FO_HORIZONTAL);
  TFT_Write_Text("EasyPIC Fusion v7", 19, 223);
  TFT_Set_Font(&Verdana12x13_Regular, CL_RED, FO_HORIZONTAL);
  TFT_Write_Text("www.mikroe.com", 200, 223);
  TFT_Set_Font(&TFT_defaultFont, CL_BLACK, FO_HORIZONTAL);
}

void Display_Temperature(unsigned int temp2write) {
  const unsigned short RES_SHIFT = TEMP_RESOLUTION - 8;
  char temp_whole;
  unsigned int temp_fraction;

  // Check if temperature is negative
  if (temp2write & 0x8000) {
    text[0] = '-';
    temp2write = ~temp2write + 1;
  }

  // Extract temp_whole
  temp_whole = temp2write >> RES_SHIFT ;

  // Convert temp_whole to characters
  if (temp_whole/100)
    text[0] = temp_whole/100  + 48;
  else
    text[0] = '0';

  text[1] = (temp_whole/10)%10 + 48;             // Extract tens digit
  text[2] =  temp_whole%10     + 48;             // Extract ones digit

  // Extract temp_fraction and convert it to unsigned int
  temp_fraction  = temp2write << (4-RES_SHIFT);
  temp_fraction &= 0x000F;
  temp_fraction *= 625;

  // Convert temp_fraction to characters
  text[4] =  temp_fraction/1000    + 48;         // Extract thousands digit
  text[5] = (temp_fraction/100)%10 + 48;         // Extract hundreds digit
  text[6] = (temp_fraction/10)%10  + 48;         // Extract tens digit
  text[7] =  temp_fraction%10      + 48;         // Extract ones digit
 
  if (strcmp(text,text_old)) {              // Compare old and new temperature
    TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_WHITE, FO_HORIZONTAL);
    TFT_Write_Text(text_old, 85, 100);           // Delete old temperature
    TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_RED, FO_HORIZONTAL);
    TFT_Write_Text(text, 85, 100);               // Display new temperature
  }
  strcpy(text_old,text);
}

void main() {
  Init();
  DrawFrame();

  TFT_Set_Font(&tahoma29x29_Bold, CL_BLACK, FO_HORIZONTAL);
  TFT_Write_Text("Temperature:  ", 75, 65);

  // Main loop
  do {
    // Perform temperature reading
    Ow_Reset(&PORTA, 0);                         // Onewire reset signal
    Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
    Ow_Write(&PORTA, 0, 0x44);                   // Issue command CONVERT_T


    Ow_Reset(&PORTA, 0);
    Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
    Ow_Write(&PORTA, 0, 0xBE);                   // Issue command READ_SCRATCHPAD

    temp =  Ow_Read(&PORTA, 0);
    temp = (Ow_Read(&PORTA, 0) << 8) + temp;

    // Format and display result on TFT
    Display_Temperature(temp);
    Delay_ms(1000);

  } while (1);
}