_ReadTempeartureDS1820:
;DS1820_driver.c,1 :: 		float ReadTempeartureDS1820(unsigned int* port, char pin){
ADDIU	SP, SP, -16
SW	RA, 0(SP)
;DS1820_driver.c,5 :: 		Ow_Reset(&PORTA, 0);                         // Onewire reset signal
SW	R25, 4(SP)
SW	R26, 8(SP)
SW	R27, 12(SP)
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Reset+0
NOP	
;DS1820_driver.c,6 :: 		Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
ORI	R27, R0, 204
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;DS1820_driver.c,7 :: 		Ow_Write(&PORTA, 0, 0x44);                   // Issue command CONVERT_T
ORI	R27, R0, 68
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;DS1820_driver.c,10 :: 		Ow_Reset(&PORTA, 0);
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Reset+0
NOP	
;DS1820_driver.c,11 :: 		Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
ORI	R27, R0, 204
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;DS1820_driver.c,12 :: 		Ow_Write(&PORTA, 0, 0xBE);                   // Issue command READ_SCRATCHPAD
ORI	R27, R0, 190
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;DS1820_driver.c,14 :: 		temp2write =  Ow_Read(&PORTA, 0);
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Read+0
NOP	
; temp2write start address is: 32 (R8)
ANDI	R8, R2, 255
;DS1820_driver.c,15 :: 		temp2write = (Ow_Read(&PORTA, 0) << 8) + temp2write;
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Read+0
NOP	
ANDI	R2, R2, 255
SLL	R2, R2, 8
ADDU	R2, R2, R8
; temp2write end address is: 32 (R8)
;DS1820_driver.c,16 :: 		return temp2write * 0.5;
ANDI	R4, R2, 65535
JAL	__Unsigned16IntToFloat+0
NOP	
LUI	R4, 16128
ORI	R4, R4, 0
MOVZ	R6, R2, R0
JAL	__Mul_FP+0
NOP	
;DS1820_driver.c,18 :: 		}
;DS1820_driver.c,16 :: 		return temp2write * 0.5;
;DS1820_driver.c,18 :: 		}
L_end_ReadTempeartureDS1820:
LW	R27, 12(SP)
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 16
JR	RA
NOP	
; end of _ReadTempeartureDS1820
