#line 1 "F:/Documents/cmb/Automer/Kantoskop.c"

sbit SDA_In_Pin at RA3_bit;
sbit SDA_Out_Pin at LATA3_bit;
sbit SCL_Pin at LATA2_bit;

sbit SDA_Direction at TRISA3_bit;
sbit SCL_Direction at TRISA2_bit;



float SHTtemperature;
float SHTrel_humidity;

void Read_SHT11(float *fT, float *fRH);

char tmp;




sbit ChipSelect at LATC4_bit;
sbit ChipSelect_Direction at TRISC4_bit;

void MCP3551_Init();
long MCP3551_Read();
double voltage;


float ReadTempeartureDS1820(char* port, char pin);

char UART_str[30];

void main() {
 CHECON = 30;
 AD1PCFG = 0xFFFF;
 AD1PCFG = 0xFFFF;
 JTAGEN_bit = 0;
 UART2_Init(256000);
 Delay_ms(100);

 SCL_Direction = 0;
 SDA_Out_Pin = 1;

 MCP3551_Init();
 Read_SHT11(&SHTtemperature, &SHTrel_humidity);

 TRISA1_bit = 0;
 LATA1_bit = 0;
 while(1){
 voltage = (MCP3551_Read() / 4194304.0) * 6.6;

 while(UART2_Data_Ready()){
 tmp = UART2_Read();
 if(tmp == 'R'){
 Read_SHT11(&SHTtemperature, &SHTrel_humidity);
 }
 if (tmp == 'S') {

 LATA1_bit = ~LATA1_bit;

 }
 }

 sprintf(UART_str,"%3.1f %3.1f %1.8f %i %2.1f \r\n", SHTtemperature, SHTrel_humidity, voltage,(int) LATA1_bit, ReadTempeartureDS1820(&PORTA, 0));

 UART2_Write_Text(UART_str);

 }
}
