_Init:
;OneWire.c,43 :: 		void Init() {
ADDIU	SP, SP, -12
SW	RA, 0(SP)
;OneWire.c,44 :: 		AD1PCFG = 0xFFFF;            // Configure AN pins as digital I/O
SW	R25, 4(SP)
SW	R26, 8(SP)
ORI	R2, R0, 65535
SW	R2, Offset(AD1PCFG+0)(GP)
;OneWire.c,45 :: 		JTAGEN_bit = 0;              // Disable JTAG
_LX	
INS	R2, R0, BitPos(JTAGEN_bit+0), 1
_SX	
;OneWire.c,46 :: 		TFT_BLED_Direction = 0;      // Set TFT backlight pin as output
LUI	R2, BitMask(TRISD2_bit+0)
ORI	R2, R2, BitMask(TRISD2_bit+0)
_SX	
;OneWire.c,47 :: 		TFT_Set_Default_Mode();
JAL	_TFT_Set_Default_Mode+0
NOP	
;OneWire.c,48 :: 		TFT_Init_ILI9341_8bit(320, 240);    // Initialize TFT display
ORI	R26, R0, 240
ORI	R25, R0, 320
JAL	_TFT_Init_ILI9341_8bit+0
NOP	
;OneWire.c,49 :: 		TFT_BLED = 1;                // Turn on TFT backlight
LUI	R2, BitMask(LATD2_bit+0)
ORI	R2, R2, BitMask(LATD2_bit+0)
_SX	
;OneWire.c,50 :: 		}
L_end_Init:
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 12
JR	RA
NOP	
; end of _Init
_DrawFrame:
;OneWire.c,52 :: 		void DrawFrame(){
ADDIU	SP, SP, -20
SW	RA, 0(SP)
;OneWire.c,53 :: 		TFT_Fill_Screen(CL_WHITE);
SW	R25, 4(SP)
SW	R26, 8(SP)
SW	R27, 12(SP)
SW	R28, 16(SP)
ORI	R25, R0, 65535
JAL	_TFT_Fill_Screen+0
NOP	
;OneWire.c,54 :: 		TFT_Set_Pen(CL_BLACK, 1);
ORI	R26, R0, 1
MOVZ	R25, R0, R0
JAL	_TFT_Set_Pen+0
NOP	
;OneWire.c,55 :: 		TFT_Line(20, 220, 300, 220);
ORI	R28, R0, 220
ORI	R27, R0, 300
ORI	R26, R0, 220
ORI	R25, R0, 20
JAL	_TFT_Line+0
NOP	
;OneWire.c,56 :: 		TFT_LIne(20,  46, 300,  46);
ORI	R28, R0, 46
ORI	R27, R0, 300
ORI	R26, R0, 46
ORI	R25, R0, 20
JAL	_TFT_Line+0
NOP	
;OneWire.c,57 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_RED, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 63488
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,58 :: 		TFT_Write_Text("Temperature  Sensor  (DS1820)", 15, 14);
ORI	R27, R0, 14
ORI	R26, R0, 15
LUI	R25, hi_addr(?lstr3_OneWire+0)
ORI	R25, R25, lo_addr(?lstr3_OneWire+0)
JAL	_TFT_Write_Text+0
NOP	
;OneWire.c,59 :: 		TFT_Set_Font(&Verdana12x13_Regular, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_Verdana12x13_Regular+0)
ORI	R25, R25, lo_addr(_Verdana12x13_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,60 :: 		TFT_Write_Text("EasyPIC Fusion v7", 19, 223);
ORI	R27, R0, 223
ORI	R26, R0, 19
LUI	R25, hi_addr(?lstr4_OneWire+0)
ORI	R25, R25, lo_addr(?lstr4_OneWire+0)
JAL	_TFT_Write_Text+0
NOP	
;OneWire.c,61 :: 		TFT_Set_Font(&Verdana12x13_Regular, CL_RED, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 63488
LUI	R25, hi_addr(_Verdana12x13_Regular+0)
ORI	R25, R25, lo_addr(_Verdana12x13_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,62 :: 		TFT_Write_Text("www.mikroe.com", 200, 223);
ORI	R27, R0, 223
ORI	R26, R0, 200
LUI	R25, hi_addr(?lstr5_OneWire+0)
ORI	R25, R25, lo_addr(?lstr5_OneWire+0)
JAL	_TFT_Write_Text+0
NOP	
;OneWire.c,63 :: 		TFT_Set_Font(&TFT_defaultFont, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_TFT_defaultFont+0)
ORI	R25, R25, lo_addr(_TFT_defaultFont+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,64 :: 		}
L_end_DrawFrame:
LW	R28, 16(SP)
LW	R27, 12(SP)
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 20
JR	RA
NOP	
; end of _DrawFrame
_Display_Temperature:
;OneWire.c,66 :: 		void Display_Temperature(unsigned int temp2write) {
ADDIU	SP, SP, -16
SW	RA, 0(SP)
;OneWire.c,72 :: 		if (temp2write & 0x8000) {
SW	R25, 4(SP)
SW	R26, 8(SP)
SW	R27, 12(SP)
ANDI	R2, R25, 32768
BNE	R2, R0, L__Display_Temperature13
NOP	
J	L_Display_Temperature0
NOP	
L__Display_Temperature13:
;OneWire.c,73 :: 		text[0] = '-';
ORI	R3, R0, 45
LW	R2, Offset(_text+0)(GP)
SB	R3, 0(R2)
;OneWire.c,74 :: 		temp2write = ~temp2write + 1;
NOR	R2, R25, R0
ADDIU	R2, R2, 1
ANDI	R25, R2, 65535
;OneWire.c,75 :: 		}
L_Display_Temperature0:
;OneWire.c,78 :: 		temp_whole = temp2write >> RES_SHIFT ;
ANDI	R2, R25, 65535
SRL	R3, R2, 1
; temp_whole start address is: 20 (R5)
ANDI	R5, R3, 65535
;OneWire.c,81 :: 		if (temp_whole/100)
ORI	R2, R0, 100
DIVU	R3, R2
MFLO	R2
BNE	R2, R0, L__Display_Temperature15
NOP	
J	L_Display_Temperature1
NOP	
L__Display_Temperature15:
;OneWire.c,82 :: 		text[0] = temp_whole/100  + 48;
ORI	R2, R0, 100
DIVU	R5, R2
MFLO	R2
ADDIU	R3, R2, 48
LW	R2, Offset(_text+0)(GP)
SB	R3, 0(R2)
J	L_Display_Temperature2
NOP	
L_Display_Temperature1:
;OneWire.c,84 :: 		text[0] = '0';
ORI	R3, R0, 48
LW	R2, Offset(_text+0)(GP)
SB	R3, 0(R2)
L_Display_Temperature2:
;OneWire.c,86 :: 		text[1] = (temp_whole/10)%10 + 48;             // Extract tens digit
LW	R2, Offset(_text+0)(GP)
ADDIU	R4, R2, 1
ORI	R2, R0, 10
DIVU	R5, R2
MFLO	R3
ORI	R2, R0, 10
DIVU	R3, R2
MFHI	R2
ADDIU	R2, R2, 48
SB	R2, 0(R4)
;OneWire.c,87 :: 		text[2] =  temp_whole%10     + 48;             // Extract ones digit
LW	R2, Offset(_text+0)(GP)
ADDIU	R3, R2, 2
ORI	R2, R0, 10
DIVU	R5, R2
MFHI	R2
; temp_whole end address is: 20 (R5)
ADDIU	R2, R2, 48
SB	R2, 0(R3)
;OneWire.c,90 :: 		temp_fraction  = temp2write << (4-RES_SHIFT);
ANDI	R2, R25, 65535
SLL	R2, R2, 3
;OneWire.c,91 :: 		temp_fraction &= 0x000F;
ANDI	R3, R2, 15
;OneWire.c,92 :: 		temp_fraction *= 625;
ORI	R2, R0, 625
MULTU	R3, R2
MFLO	R4
; temp_fraction start address is: 20 (R5)
ANDI	R5, R4, 65535
;OneWire.c,95 :: 		text[4] =  temp_fraction/1000    + 48;         // Extract thousands digit
LW	R2, Offset(_text+0)(GP)
ADDIU	R3, R2, 4
ORI	R2, R0, 1000
DIVU	R4, R2
MFLO	R2
ADDIU	R2, R2, 48
SB	R2, 0(R3)
;OneWire.c,96 :: 		text[5] = (temp_fraction/100)%10 + 48;         // Extract hundreds digit
LW	R2, Offset(_text+0)(GP)
ADDIU	R4, R2, 5
ORI	R2, R0, 100
DIVU	R5, R2
MFLO	R3
ORI	R2, R0, 10
DIVU	R3, R2
MFHI	R2
ADDIU	R2, R2, 48
SB	R2, 0(R4)
;OneWire.c,97 :: 		text[6] = (temp_fraction/10)%10  + 48;         // Extract tens digit
LW	R2, Offset(_text+0)(GP)
ADDIU	R4, R2, 6
ORI	R2, R0, 10
DIVU	R5, R2
MFLO	R3
ORI	R2, R0, 10
DIVU	R3, R2
MFHI	R2
ADDIU	R2, R2, 48
SB	R2, 0(R4)
;OneWire.c,98 :: 		text[7] =  temp_fraction%10      + 48;         // Extract ones digit
LW	R2, Offset(_text+0)(GP)
ADDIU	R3, R2, 7
ORI	R2, R0, 10
DIVU	R5, R2
MFHI	R2
; temp_fraction end address is: 20 (R5)
ADDIU	R2, R2, 48
SB	R2, 0(R3)
;OneWire.c,100 :: 		if (strcmp(text,text_old)) {              // Compare old and new temperature
LW	R26, Offset(_text_old+0)(GP)
LW	R25, Offset(_text+0)(GP)
JAL	_strcmp+0
NOP	
BNE	R2, R0, L__Display_Temperature17
NOP	
J	L_Display_Temperature3
NOP	
L__Display_Temperature17:
;OneWire.c,101 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_WHITE, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 65535
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,102 :: 		TFT_Write_Text(text_old, 85, 100);           // Delete old temperature
ORI	R27, R0, 100
ORI	R26, R0, 85
LW	R25, Offset(_text_old+0)(GP)
JAL	_TFT_Write_Text+0
NOP	
;OneWire.c,103 :: 		TFT_Set_Font(&HandelGothic_BT21x22_Regular, CL_RED, FO_HORIZONTAL);
MOVZ	R27, R0, R0
ORI	R26, R0, 63488
LUI	R25, hi_addr(_HandelGothic_BT21x22_Regular+0)
ORI	R25, R25, lo_addr(_HandelGothic_BT21x22_Regular+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,104 :: 		TFT_Write_Text(text, 85, 100);               // Display new temperature
ORI	R27, R0, 100
ORI	R26, R0, 85
LW	R25, Offset(_text+0)(GP)
JAL	_TFT_Write_Text+0
NOP	
;OneWire.c,105 :: 		}
L_Display_Temperature3:
;OneWire.c,106 :: 		strcpy(text_old,text);
LW	R26, Offset(_text+0)(GP)
LW	R25, Offset(_text_old+0)(GP)
JAL	_strcpy+0
NOP	
;OneWire.c,107 :: 		}
L_end_Display_Temperature:
LW	R27, 12(SP)
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 16
JR	RA
NOP	
; end of _Display_Temperature
_main:
;OneWire.c,109 :: 		void main() {
;OneWire.c,110 :: 		Init();
JAL	_Init+0
NOP	
;OneWire.c,111 :: 		DrawFrame();
JAL	_DrawFrame+0
NOP	
;OneWire.c,113 :: 		TFT_Set_Font(&tahoma29x29_Bold, CL_BLACK, FO_HORIZONTAL);
MOVZ	R27, R0, R0
MOVZ	R26, R0, R0
LUI	R25, hi_addr(_tahoma29x29_Bold+0)
ORI	R25, R25, lo_addr(_tahoma29x29_Bold+0)
JAL	_TFT_Set_Font+0
NOP	
;OneWire.c,114 :: 		TFT_Write_Text("Temperature:  ", 75, 65);
ORI	R27, R0, 65
ORI	R26, R0, 75
LUI	R25, hi_addr(?lstr6_OneWire+0)
ORI	R25, R25, lo_addr(?lstr6_OneWire+0)
JAL	_TFT_Write_Text+0
NOP	
;OneWire.c,117 :: 		do {
L_main4:
;OneWire.c,119 :: 		Ow_Reset(&PORTA, 0);                         // Onewire reset signal
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Reset+0
NOP	
;OneWire.c,120 :: 		Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
ORI	R27, R0, 204
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;OneWire.c,121 :: 		Ow_Write(&PORTA, 0, 0x44);                   // Issue command CONVERT_T
ORI	R27, R0, 68
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;OneWire.c,124 :: 		Ow_Reset(&PORTA, 0);
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Reset+0
NOP	
;OneWire.c,125 :: 		Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
ORI	R27, R0, 204
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;OneWire.c,126 :: 		Ow_Write(&PORTA, 0, 0xBE);                   // Issue command READ_SCRATCHPAD
ORI	R27, R0, 190
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Write+0
NOP	
;OneWire.c,128 :: 		temp =  Ow_Read(&PORTA, 0);
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Read+0
NOP	
ANDI	R2, R2, 255
SH	R2, Offset(_temp+0)(GP)
;OneWire.c,129 :: 		temp = (Ow_Read(&PORTA, 0) << 8) + temp;
MOVZ	R26, R0, R0
LUI	R25, hi_addr(PORTA+0)
ORI	R25, R25, lo_addr(PORTA+0)
JAL	_Ow_Read+0
NOP	
ANDI	R2, R2, 255
SLL	R3, R2, 8
LHU	R2, Offset(_temp+0)(GP)
ADDU	R2, R3, R2
SH	R2, Offset(_temp+0)(GP)
;OneWire.c,132 :: 		Display_Temperature(temp);
ANDI	R25, R2, 65535
JAL	_Display_Temperature+0
NOP	
;OneWire.c,133 :: 		Delay_ms(1000);
LUI	R24, 406
ORI	R24, R24, 59050
L_main7:
ADDIU	R24, R24, -1
BNE	R24, R0, L_main7
NOP	
;OneWire.c,135 :: 		} while (1);
J	L_main4
NOP	
;OneWire.c,136 :: 		}
L_end_main:
L__main_end_loop:
J	L__main_end_loop
NOP	
; end of _main
