float ReadTempeartureDS1820(unsigned int* port, char pin){
    unsigned temp2write;
    
    // Perform temperature reading
    Ow_Reset(&PORTA, 0);                         // Onewire reset signal
    Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
    Ow_Write(&PORTA, 0, 0x44);                   // Issue command CONVERT_T


    Ow_Reset(&PORTA, 0);
    Ow_Write(&PORTA, 0, 0xCC);                   // Issue command SKIP_ROM
    Ow_Write(&PORTA, 0, 0xBE);                   // Issue command READ_SCRATCHPAD

    temp2write =  Ow_Read(&PORTA, 0);
    temp2write = (Ow_Read(&PORTA, 0) << 8) + temp2write;
    return temp2write * 0.5;

}
