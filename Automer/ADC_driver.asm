_MCP3551_Init:
;ADC_driver.c,3 :: 		void MCP3551_Init(){
ADDIU	SP, SP, -20
SW	RA, 0(SP)
;ADC_driver.c,6 :: 		ChipSelect_Direction = 0;
SW	R25, 4(SP)
SW	R26, 8(SP)
SW	R27, 12(SP)
SW	R28, 16(SP)
_LX	
INS	R2, R0, BitPos(ChipSelect_Direction+0), 1
_SX	
;ADC_driver.c,10 :: 		SPI2_Init_Advanced(_SPI_MASTER, _SPI_8_BIT, 16, _SPI_SS_DISABLE, _SPI_DATA_SAMPLE_END, _SPI_CLK_IDLE_HIGH, _SPI_ACTIVE_2_IDLE);
MOVZ	R28, R0, R0
ORI	R27, R0, 16
MOVZ	R26, R0, R0
ORI	R25, R0, 32
ADDIU	SP, SP, -8
SH	R0, 4(SP)
ORI	R2, R0, 64
SH	R2, 2(SP)
ORI	R2, R0, 512
SH	R2, 0(SP)
JAL	_SPI2_Init_Advanced+0
NOP	
ADDIU	SP, SP, 8
;ADC_driver.c,11 :: 		Delay_ms(100);                       // Small brake for SPI to initialize
LUI	R24, 40
ORI	R24, R24, 45226
L_MCP3551_Init0:
ADDIU	R24, R24, -1
BNE	R24, R0, L_MCP3551_Init0
NOP	
;ADC_driver.c,12 :: 		}
L_end_MCP3551_Init:
LW	R28, 16(SP)
LW	R27, 12(SP)
LW	R26, 8(SP)
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 20
JR	RA
NOP	
; end of _MCP3551_Init
_MCP3551_Read:
;ADC_driver.c,15 :: 		long MCP3551_Read() {
ADDIU	SP, SP, -8
SW	RA, 0(SP)
;ADC_driver.c,16 :: 		char ADC_Byte1, ADC_Byte2, ADC_Byte3, timer = 0;
SW	R25, 4(SP)
; timer start address is: 12 (R3)
MOVZ	R3, R0, R0
;ADC_driver.c,17 :: 		long ADC_Value = 0;
;ADC_driver.c,19 :: 		ChipSelect = 0;                      // CS_pin; Clear(port bit); put low
_LX	
INS	R2, R0, BitPos(ChipSelect+0), 1
_SX	
;ADC_driver.c,20 :: 		Delay_us(100);                       // PowerUp Time (min.10us)
LUI	R24, 0
ORI	R24, R24, 2666
L_MCP3551_Read2:
ADDIU	R24, R24, -1
BNE	R24, R0, L_MCP3551_Read2
NOP	
; timer end address is: 12 (R3)
;ADC_driver.c,22 :: 		while(RG7_bit) {                     // Conversion time: 75ms
L_MCP3551_Read4:
; timer start address is: 12 (R3)
_LX	
EXT	R2, R2, BitPos(RG7_bit+0), 1
BNE	R2, R0, L__MCP3551_Read32
NOP	
J	L_MCP3551_Read5
NOP	
L__MCP3551_Read32:
;ADC_driver.c,24 :: 		ChipSelect = 1;                    // CS_pin; Set(port bit); put high
_LX	
ORI	R2, R2, BitMask(ChipSelect+0)
_SX	
;ADC_driver.c,25 :: 		Delay_us(1);
LUI	R24, 0
ORI	R24, R24, 26
L_MCP3551_Read6:
ADDIU	R24, R24, -1
BNE	R24, R0, L_MCP3551_Read6
NOP	
;ADC_driver.c,26 :: 		ChipSelect = 0;                    // CS_pin; Clear(port bit); put low
_LX	
INS	R2, R0, BitPos(ChipSelect+0), 1
_SX	
;ADC_driver.c,27 :: 		timer++;
ADDIU	R2, R3, 1
ANDI	R3, R2, 255
;ADC_driver.c,28 :: 		Delay_ms(10);                      // Polling SDI_bit on every 10ms
LUI	R24, 4
ORI	R24, R24, 4522
L_MCP3551_Read8:
ADDIU	R24, R24, -1
BNE	R24, R0, L_MCP3551_Read8
NOP	
;ADC_driver.c,30 :: 		if (timer >= 20) {                 // Failsafe timer
ANDI	R2, R3, 255
SLTIU	R2, R2, 20
BEQ	R2, R0, L__MCP3551_Read33
NOP	
J	L_MCP3551_Read10
NOP	
L__MCP3551_Read33:
; timer end address is: 12 (R3)
;ADC_driver.c,31 :: 		return -400000;                 // No response from MCP3551;
LUI	R2, 65529
ORI	R2, R2, 58752
J	L_end_MCP3551_Read
NOP	
;ADC_driver.c,32 :: 		}
L_MCP3551_Read10:
;ADC_driver.c,33 :: 		}
; timer start address is: 12 (R3)
; timer end address is: 12 (R3)
J	L_MCP3551_Read4
NOP	
L_MCP3551_Read5:
;ADC_driver.c,34 :: 		ADC_Byte3 = SPI2_Read(0);
MOVZ	R25, R0, R0
JAL	_SPI2_Read+0
NOP	
; ADC_Byte3 start address is: 16 (R4)
MOVZ	R4, R2, R0
;ADC_driver.c,35 :: 		ADC_Byte2 = SPI2_Read(0);
MOVZ	R25, R0, R0
JAL	_SPI2_Read+0
NOP	
; ADC_Byte2 start address is: 24 (R6)
MOVZ	R6, R2, R0
;ADC_driver.c,36 :: 		ADC_Byte1 = SPI2_Read(0);
MOVZ	R25, R0, R0
JAL	_SPI2_Read+0
NOP	
; ADC_Byte1 start address is: 20 (R5)
MOVZ	R5, R2, R0
;ADC_driver.c,39 :: 		ChipSelect = 1;                      // CS_pin; Set(port bit); put high
_LX	
ORI	R2, R2, BitMask(ChipSelect+0)
_SX	
;ADC_driver.c,40 :: 		Delay_us(100);
LUI	R24, 0
ORI	R24, R24, 2666
L_MCP3551_Read11:
ADDIU	R24, R24, -1
BNE	R24, R0, L_MCP3551_Read11
NOP	
;ADC_driver.c,43 :: 		ADC_Value = ADC_Byte3;
; ADC_Value start address is: 8 (R2)
ANDI	R2, R4, 255
;ADC_driver.c,44 :: 		ADC_Value = (ADC_Value << 8) | ADC_Byte2;
SLL	R3, R2, 8
; ADC_Value end address is: 8 (R2)
ANDI	R2, R6, 255
; ADC_Byte2 end address is: 24 (R6)
OR	R2, R3, R2
;ADC_driver.c,45 :: 		ADC_Value = (ADC_Value << 8) | ADC_Byte1;
SLL	R3, R2, 8
ANDI	R2, R5, 255
OR	R2, R3, R2
; ADC_Value start address is: 24 (R6)
MOVZ	R6, R2, R0
;ADC_driver.c,48 :: 		if (ADC_Byte3 == 0x60 && ADC_Byte1 > 0)
ANDI	R3, R4, 255
ORI	R2, R0, 96
BEQ	R3, R2, L__MCP3551_Read34
NOP	
J	L__MCP3551_Read25
NOP	
L__MCP3551_Read34:
ANDI	R2, R5, 255
SLTIU	R2, R2, 1
BEQ	R2, R0, L__MCP3551_Read35
NOP	
J	L__MCP3551_Read24
NOP	
L__MCP3551_Read35:
; ADC_Byte3 end address is: 16 (R4)
; ADC_Byte1 end address is: 20 (R5)
; ADC_Value end address is: 24 (R6)
L__MCP3551_Read23:
;ADC_driver.c,49 :: 		ADC_Value = 3000000;               // Vin+ >= Vref ; Vin- = GND
; ADC_Value start address is: 12 (R3)
LUI	R3, 45
ORI	R3, R3, 50880
; ADC_Value end address is: 12 (R3)
J	L_MCP3551_Read16
NOP	
;ADC_driver.c,48 :: 		if (ADC_Byte3 == 0x60 && ADC_Byte1 > 0)
L__MCP3551_Read25:
; ADC_Value start address is: 24 (R6)
; ADC_Byte1 start address is: 20 (R5)
; ADC_Byte3 start address is: 16 (R4)
L__MCP3551_Read24:
;ADC_driver.c,52 :: 		else if (ADC_Byte3 == 0x9F && ADC_Byte1 > 0){
ANDI	R3, R4, 255
ORI	R2, R0, 159
BEQ	R3, R2, L__MCP3551_Read36
NOP	
J	L__MCP3551_Read27
NOP	
L__MCP3551_Read36:
ANDI	R2, R5, 255
; ADC_Byte1 end address is: 20 (R5)
SLTIU	R2, R2, 1
BEQ	R2, R0, L__MCP3551_Read37
NOP	
J	L__MCP3551_Read26
NOP	
L__MCP3551_Read37:
; ADC_Byte3 end address is: 16 (R4)
; ADC_Value end address is: 24 (R6)
L__MCP3551_Read22:
;ADC_driver.c,53 :: 		ADC_Value = -3000000;              // Vin- >= Vref ; Vin+ = GND
; ADC_Value start address is: 8 (R2)
LUI	R2, 65490
ORI	R2, R2, 14656
;ADC_driver.c,54 :: 		}
MOVZ	R3, R2, R0
; ADC_Value end address is: 8 (R2)
J	L_MCP3551_Read20
NOP	
;ADC_driver.c,52 :: 		else if (ADC_Byte3 == 0x9F && ADC_Byte1 > 0){
L__MCP3551_Read27:
; ADC_Value start address is: 24 (R6)
; ADC_Byte3 start address is: 16 (R4)
L__MCP3551_Read26:
;ADC_driver.c,57 :: 		if ((ADC_Byte3 & 0x20) >> 5)       // Bit 21 = 1 ; value is negative
ANDI	R2, R4, 32
; ADC_Byte3 end address is: 16 (R4)
ANDI	R2, R2, 255
SRL	R2, R2, 5
BNE	R2, R0, L__MCP3551_Read39
NOP	
J	L__MCP3551_Read28
NOP	
L__MCP3551_Read39:
;ADC_driver.c,58 :: 		ADC_Value = ADC_Value - 4194304;  // for 22 bit resolution
LUI	R2, 64
SUBU	R2, R6, R2
; ADC_Value end address is: 24 (R6)
; ADC_Value start address is: 12 (R3)
MOVZ	R3, R2, R0
; ADC_Value end address is: 12 (R3)
MOVZ	R2, R3, R0
J	L_MCP3551_Read21
NOP	
L__MCP3551_Read28:
;ADC_driver.c,57 :: 		if ((ADC_Byte3 & 0x20) >> 5)       // Bit 21 = 1 ; value is negative
MOVZ	R2, R6, R0
;ADC_driver.c,58 :: 		ADC_Value = ADC_Value - 4194304;  // for 22 bit resolution
L_MCP3551_Read21:
;ADC_driver.c,59 :: 		}
; ADC_Value start address is: 8 (R2)
MOVZ	R3, R2, R0
; ADC_Value end address is: 8 (R2)
L_MCP3551_Read20:
; ADC_Value start address is: 12 (R3)
; ADC_Value end address is: 12 (R3)
L_MCP3551_Read16:
;ADC_driver.c,61 :: 		return ADC_Value;
; ADC_Value start address is: 12 (R3)
MOVZ	R2, R3, R0
; ADC_Value end address is: 12 (R3)
;ADC_driver.c,63 :: 		}
;ADC_driver.c,61 :: 		return ADC_Value;
;ADC_driver.c,63 :: 		}
L_end_MCP3551_Read:
LW	R25, 4(SP)
LW	RA, 0(SP)
ADDIU	SP, SP, 8
JR	RA
NOP	
; end of _MCP3551_Read
