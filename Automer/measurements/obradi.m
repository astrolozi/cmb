close all
[time,Tout,Hput,Vout,ON,Tin,Ub,Angle] = importfile(filename,1,-1);
angle = 154 - Angle;
sec = 1./cos(angle .* (pi / 180));

plot(sec, Vout - Ub, 'b.');
title('Grafik zavisnosti izmerene snage od sekansa zenitne daljine');
xlabel('sec(Y)');
ylabel('U [V]');
saveas(gcf, strcat(filename, 'fig1.png'));
saveas(gcf, strcat(filename, 'fig1.fig'));
saveas(gcf, strcat(filename, 'fig1.emf'));

figure, plot(Vout, 'b.');
hold on, plot(Ub, 'r.');
hold on, plot(Tin * 10e-3 + 1.2, 'g.');
hold on, plot(angle * 10e-3, 'g.');
title('Grafik merenih vrednosti');
xlabel('Sempl');
ylabel('U[V], T[degC * 10^-2 + 1.2], Z[deg * 10^-2]');
legend('Izlazni napon', 'Napon kalibracije', 'Temperatura meraca snage', 'Zenitna daljina');
saveas(gcf, strcat(filename, 'fig2.png'));
saveas(gcf, strcat(filename, 'fig2.fig'));
saveas(gcf, strcat(filename, 'fig2.emf'));

figure, plot(angle, Vout-Ub, 'b.');
title('Grafik zavisnosti izmerene snage od zenitne daljine');
xlabel('Zenitna daljina [deg]');
ylabel('U [V]');
saveas(gcf, strcat(filename, 'fig3.png'));
saveas(gcf, strcat(filename, 'fig3.fig'));
saveas(gcf, strcat(filename, 'fig3.emf'));