﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uglomer
{
    public partial class Form1 : Form
    {
        double ub = 100;
        double angle = 0;
        DateTime last = DateTime.MinValue;
        double trustCoeff = 0.99;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        void ComplementaryFilter(string next)
        {
            var outputs = next.Split(' ');
            if (outputs.Length == 8)
            {
                DateTime current = DateTime.Parse(outputs[1]);
                if (last != DateTime.MinValue)
                {
                    current = DateTime.Parse(outputs[1]);
                    var diff1 = current - last;
                    var diff = diff1.TotalMilliseconds;
                    angle = trustCoeff *  Math.Atan2(Convert.ToDouble(outputs[3]), Convert.ToDouble(outputs[4])) * 180 / Math.PI + (1-trustCoeff) * angle;
                    last = current;
                }
                else
                {
                    last = current;
                    angle = Math.Atan2(Convert.ToDouble(outputs[3]), Convert.ToDouble(outputs[4])) * 180 / Math.PI;

                }
            }

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(openFileDialog1.FileName + ".obr.txt");
            while (!sr.EndOfStream)
            {
                var input = sr.ReadLine();
                var inputs = input.Split(';');
                double tmpUB = 0, UBcnt = 0;
                bool bias = false;
                for (int i = 1; i < inputs.Length; i++)
                {
                    ComplementaryFilter(inputs[i]);
                }
                try {
                    if (inputs[0].Split(' ')[4] == "0")
                    {
                        if (bias)
                        {
                            tmpUB += Convert.ToDouble(inputs[0].Split(' ')[3]);
                            UBcnt++;
                            ub = tmpUB / UBcnt;
                        }
                        else
                        {
                            tmpUB = Convert.ToDouble(inputs[0].Split(' ')[3]);
                            UBcnt = 1;
                            ub = tmpUB;
                        }
                    }
                    else if (inputs[0].Split(' ')[4] == "1" && ub != 100 && Math.Abs(Convert.ToDouble(inputs[0].Split(' ')[3]) - ub) > 10e-4)
                    {
                        sw.WriteLine(inputs[0] + ub.ToString() + " " + angle.ToString());
                    }
                } catch (Exception ex) { 
}
            }
            sw.Close();
            sr.Close();
        }
    }
}
