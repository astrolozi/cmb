#line 1 "F:/Documents/cmb/Automer/ADC_driver.c"
extern sfr sbit ChipSelect;
extern sfr sbit ChipSelect_Direction;
void MCP3551_Init(){


 ChipSelect_Direction = 0;



 SPI2_Init_Advanced(_SPI_MASTER, _SPI_8_BIT, 16, _SPI_SS_DISABLE, _SPI_DATA_SAMPLE_END, _SPI_CLK_IDLE_HIGH, _SPI_ACTIVE_2_IDLE);
 Delay_ms(100);
}


long MCP3551_Read() {
 char ADC_Byte1, ADC_Byte2, ADC_Byte3, timer = 0;
 long ADC_Value = 0;

 ChipSelect = 0;
 Delay_us(100);

 while(RG7_bit) {

 ChipSelect = 1;
 Delay_us(1);
 ChipSelect = 0;
 timer++;
 Delay_ms(10);

 if (timer >= 20) {
 return -400000;
 }
 }
 ADC_Byte3 = SPI2_Read(0);
 ADC_Byte2 = SPI2_Read(0);
 ADC_Byte1 = SPI2_Read(0);


 ChipSelect = 1;
 Delay_us(100);


 ADC_Value = ADC_Byte3;
 ADC_Value = (ADC_Value << 8) | ADC_Byte2;
 ADC_Value = (ADC_Value << 8) | ADC_Byte1;


 if (ADC_Byte3 == 0x60 && ADC_Byte1 > 0)
 ADC_Value = 3000000;


 else if (ADC_Byte3 == 0x9F && ADC_Byte1 > 0){
 ADC_Value = -3000000;
 }

 else {
 if ((ADC_Byte3 & 0x20) >> 5)
 ADC_Value = ADC_Value - 4194304;
 }

 return ADC_Value;

}
